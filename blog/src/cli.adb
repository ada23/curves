with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Strings.Fixed ;

with GNAT.Command_Line;
with GNAT.Source_Info; use GNAT.Source_Info;

with hex ;

package body cli is

   package boolean_text_io is new Enumeration_IO (Boolean);
   use boolean_text_io;

   procedure SwitchHandler
     (Switch : String; Parameter : String; Section : String)
   is
   begin
      Put ("SwitchHandler " & Switch);
      Put (" Parameter " & Parameter);
      Put (" Section " & Section);
      New_Line;
   end SwitchHandler;

   procedure ProcessCommandLine is
      Config : GNAT.Command_Line.Command_Line_Configuration;
   begin

      GNAT.Command_Line.Set_Usage
        (Config,
         Help =>
           NAME & " " & VERSION & " " & Compilation_ISO_Date & " " &
           Compilation_Time,
         Usage => "subcommands: playback|plot binfile1 binfile2 ...  .use --help with the subcommand");

      GNAT.Command_Line.Initialize_Option_Scan(Stop_At_First_Non_Switch => true );
      GNAT.Command_Line.Define_Switch
        (Config, Verbosity'access, Switch => "-v:", Long_Switch => "--verbosity:",
         Help                           => "Verbosity Level");
      GNAT.Command_Line.Define_Switch
        (Config, IdFilter'access, Switch => "-i:", Long_Switch => "--id-filter:",
         Help                           => "Message id filter hexid,hexid,hexid");   
      

      GNAT.Command_Line.Getopt (Config, SwitchHandler'access);
      declare
         subcommand : string := GNAT.Command_Line.Get_Argument (Do_Expansion => False);
      begin
         if subcommand = "alarm"
         then
            GNAT.Command_Line.Set_Usage( config ,
                                         "process alarm records"
                                        );
            command := alarm ;
            
            GNAT.Command_Line.Define_Switch
              (Config, activeOption'Access , Switch => "-a" ,
               Long_Switch => "--active", Help => "Active alarms. Default: all alarms" );
            
            GNAT.Command_Line.Define_Switch
              (Config, clearedOption'Access , Switch => "-c" ,
               Long_Switch => "--cleared", Help => "Declare+Clear paired alarms" );
            
            GNAT.Command_Line.Define_Switch
              (Config, fromDevice'Access , Switch => "-f=" ,
               Long_Switch => "--from=", Help => "filter alarms from this device only. (bg,mc,pss,th)" );        
            
        elsif subcommand = "thermal"
         then
            GNAT.Command_Line.Set_Usage( config ,
                                         "thermal system records"
                                        );
            command := thermal ;
            
         elsif subcommand = "playback"
         then
            GNAT.Command_Line.Set_Usage( config ,
                                         "create a cleaner version of the log"
                                        );
            command := playback ;
            GNAT.Command_Line.Define_Switch
              (Config, target'Access , Switch => "-t=",
               Long_Switch => "--target=", Help => "Socket target port@ip" );
            
            GNAT.Command_Line.Define_Switch
              (Config, UDPOption'Access , Switch => "-u",
               Long_Switch => "--udp", Help => "UDP protocol. Default => Stream" );
            
         elsif subcommand = "plot"
         then
            GNAT.Command_Line.Set_Usage( config ,
                                         "plot the signals from the config file"
                                        );
            command := plot ;

         elsif subcommand = "convert"
         then
            Put_Line("Convert");
            GNAT.Command_Line.Set_Usage( config ,
                                         "convert the input file "
                                        );
            command := convert ;

         elsif subcommand = "help"
         then
            GNAT.Command_Line.Set_Usage( config ,
                                         "show predefined ids"
                                        );
            command := help ;
            Put_Line("Help command");
         end if ;
      end ;

      GNAT.Command_Line.Initialize_Option_Scan(Stop_At_First_Non_Switch => false );
      GNAT.Command_Line.Getopt (Config, SwitchHandler'access);

      if IdFilter.all'Length > 0
      then
         ParseMessageIds( IdFilter.all ) ;
      end if ;
      
   end ProcessCommandLine;

   function GetNextArgument return String is
   begin
      return GNAT.Command_Line.Get_Argument (Do_Expansion => True);
   end GetNextArgument;

   function GatherArguments return Args_Pkg.Vector is
      result : Args_Pkg.Vector ;
   begin
      Declare
         cmdarg : String := GetNextArgument;
      begin
         null ;
      end ;
      loop
         declare
            arg : Ada.Strings.Unbounded.Unbounded_String := To_Unbounded_String( GetNextArgument ) ;
         begin
            result.Append(arg) ;
         end ;
      end loop ;
   exception
      when others =>
         return result ;
   end GatherArguments ;
   
   procedure ShowArgument( arg : Args_Pkg.Cursor ) is
      argstr : Unbounded_String ;
   begin

      argstr := Args_Pkg.Element( arg ) ;
      if Length(argstr) > 0
      then
         Put_Line(To_String(argstr));
      end if ;
      
   end ShowArgument ;
   procedure ShowArguments( arglist : Args_Pkg.Vector ) is
   begin
      Args_Pkg.Iterate( arglist , ShowArgument'access ) ;
   end ShowArguments ;
   
   function Get(Prompt : string) return string is
      result : String(1..80);
      len : natural ;
   begin
      Ada.Text_Io.Put(Prompt) ; Ada.Text_Io.Put(" > "); Ada.Text_Io.Flush ;
      Ada.Text_Io.Get_Line(result,len);
      return result(1..len);
   end Get ;

   ----------------------------------------------------------------------------
   function Listed( msg_id : Unsigned_16 ) return boolean is
      use Ids_Pkg ;
      cursor : Ids_Pkg.Cursor ;
   begin
      Cursor := Ids_Pkg.Find( FilteredIds , msg_id ) ;
      if Cursor /= Ids_Pkg.No_Element
      then
         return true ;
      end if ;
      return false ;
   end Listed ;
   
   
   procedure ParseMessageIds( ids : string ) is
      st : integer := ids'first ;
      idx : natural ;
      procedure Add_Id(ids : string ) is
         newid : string(1..4) := ids ;
      begin
         FilteredIds.Append(  hex.Value(newid) ) ;
      end Add_Id ;      
   begin
      while st < ids'last
      loop
         idx := Ada.Strings.Fixed.Index( ids , "," , st );
         if idx = 0
         then
            if st < ids'last
            then
               Put_Line( ids(st..ids'last) ) ;
               Add_Id(ids(st..ids'last));
               st := ids'last + 1; 
            end if ;
         else
            Put_Line( ids(st..idx-1) );
            Add_Id(ids(st..idx-1));
            st := idx + 1 ;
         end if ;
      end loop ;
   end ParseMessageIds ;
end cli;
