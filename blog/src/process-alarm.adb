with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Interfaces; use Interfaces;
with Interfaces.C.Extensions ;

with hex;
with alarm_defs_h ;
with alarm_h ;

package body process.alarm is


   
   procedure Exec( p : in out AlarmProcessor ; msg : messages_h.mm_queue_msg) is
   begin
      ShowHeader( msg ) ;
      case Integer(msg.parent.alarm.hdr.msg_field.msg_type) is
         when messages_h.DATA_LOG_DATE_TIME_REFERENCE =>
            -- Put("--------------------- ");
            Show_DataLog_TimeStamp (msg) ;
         when messages_h.ALARM_SET_REQUEST | 
              messages_h.ALARM_SET_SUCCESS | 
              messages_h.ALARM_SET_FAILURE | 
              messages_h.ALARM_GET_REQUEST | 
              messages_h.ALARM_GET_SUCCESS | 
              messages_h.ALARM_GET_FAILURE => 
            MsgUsedLen := messages_h.mm_queue_msg_printf(MsgImage'Address, MsgImage'Length , msg.parent.alarm'Address ) ;
            Put_Line(MsgImage(1..Integer(MsgUsedLen) ));
         when others =>
            null ;
            -- ShowHeader(msg) ; New_Line;
      end case ;
      New_Line;
   end Exec ;
   procedure Finalize( p : in out AlarmProcessor ) is
   begin
      null ;
   end Finalize ;
   
end process.alarm;
