with System ;
with Interfaces.C ;
with Interfaces.C.Strings ;

with cli ; use cli;
with messages_h; use messages_h ;
package process is

   procedure Initialize ;
   
   type Processor is abstract tagged
      record
         null ;
      end record ;
   procedure Exec( p : in out Processor ; msg : messages_h.mm_queue_msg ) is abstract ;
   procedure Finalize( p : in out Processor ) is abstract ;
   
   function Create(name : string) return Processor'Class ;
   
   procedure Exec( args : Args_Pkg.Vector ; p : in out Processor'Class );
   
   -- Helper Routines
   procedure ShowHeader( msg : messages_h.mm_queue_msg ) ;
   procedure Show_DataLog_Timestamp (msg : messages_h.mm_queue_msg) ;
       
private
   MsgImage : String( 1..156) ;
   MsgUsedLen : Interfaces.C.int ;  
end process;
