package process.thermal is

   type ThermalProcessor is new Processor with null record ;
   
   procedure Exec( p : in out ThermalProcessor ; msg : messages_h.mm_queue_msg ) ;
   procedure Finalize( p : in out ThermalProcessor ) ;

end process.thermal;
