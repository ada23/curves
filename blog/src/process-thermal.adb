with Ada.Text_Io; use Ada.Text_Io;

package body process.thermal is
   procedure Exec( p : in out ThermalProcessor ; msg : messages_h.mm_queue_msg ) is
   begin


      case Integer(msg.parent.alarm.hdr.msg_field.msg_type) is
         when messages_h.DATA_LOG_DATE_TIME_REFERENCE =>
              Show_DataLog_TimeStamp (msg) ;
         when TH_POD_TEMPERATURE_SET_REQUEST |
              TH_POD_TEMPERATURE_SET_SUCCESS |
              TH_POD_TEMPERATURE_SET_FAILURE |
              TH_POD_TEMPERATURE_GET_REQUEST |
              TH_POD_TEMPERATURE_GET_SUCCESS |
              TH_POD_TEMPERATURE_GET_FAILURE =>
            ShowHeader( msg ) ;
            MsgUsedLen := mm_queue_msg_printf(MsgImage'Address, MsgImage'Length , msg.parent.alarm'Address ) ;
            Put_Line(MsgImage(1..Integer(MsgUsedLen) ));
         when others =>
            null ;
            -- ShowHeader(msg) ; New_Line;
      end case ;

   end Exec ;

   procedure Finalize( p : in out ThermalProcessor ) is
   begin
      null ;
   end Finalize ;

end process.thermal ;
