
package process.playback is

   type PlaybackProcessor is new Processor with null record ;
   procedure Exec( p : in out PlaybackProcessor ; msg : messages_h.mm_queue_msg ) ;
   procedure Finalize( p : in out PlaybackProcessor ) ;
end process.playback;
