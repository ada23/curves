with Text_Io; use Text_Io;

with cli ;
with process ;

with Process.Playback ;
with process.alarm ;

procedure Blog is
  arglist : cli.Args_Pkg.Vector ;
begin
   --  Insert code here.
   cli.ProcessCommandLine;
   Put_Line( cli.command'Image );
   arglist := cli.GatherArguments ;
   if cli.verbosity > 0
   then
      cli.ShowArguments(arglist);
   end if ;
   case cli.command is
      when cli.playback =>
         -- Process.Playback.Exec( arglist ) ;
         null;
      when cli.alarm =>
         declare
            proc : process.Processor'Class := process.Create("alarm");
         begin
            process.Exec( arglist , proc );
         end ;
      when cli.thermal =>
         declare
            proc : process.Processor'Class := process.Create("thermal");
         begin
            process.Exec( arglist , proc ) ;
         end ;
      when others =>
         Put_Line("Unsupported command ");
   end case ;

end Blog;
