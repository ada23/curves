with Interfaces ; use Interfaces ;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded ;
with Ada.Containers.Vectors ;

with gnat.strings ;

package cli is

   VERSION : string := "V01" ;
   NAME : String := "blog" ;
   
   type max_arguments is range 1..32 ;
   package Args_Pkg is new Ada.Containers.Vectors( max_arguments , Ada.Strings.Unbounded.Unbounded_String ) ;
   package Ids_Pkg is new Ada.Containers.Vectors( max_arguments , Interfaces.Unsigned_16 ) ;
   
   type commands is
     (
      undefined ,
      alarm ,
      thermal ,
      playback ,
      plot ,
      convert ,
      help ) ;

   command : commands ;
      
   Verbosity : aliased Integer ;

   -- Alarm Options
   activeOption : aliased boolean ;
   clearedOption : aliased boolean ;
   fromDevice : aliased GNAT.Strings.String_Access ;
   
   -- Playback Options
   target : aliased GNAT.Strings.String_Access ;            -- Target for playback
                                                            -- port@ip
   UDPOption : aliased boolean ;
   

   FilteredIds : Ids_Pkg.Vector ;
   
   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   function GatherArguments return Args_Pkg.Vector ;
   procedure ShowArgument( arg : Args_Pkg.Cursor ) ;
   procedure ShowArguments( arglist : Args_Pkg.Vector );
   
   function Listed( msg_id : Unsigned_16 ) return boolean ;
   
   function Get(prompt : string) return String ;
   
private
   IdFilter : aliased GNAT.Strings.String_Access ;
   procedure ParseMessageIds( ids : string ) ;
   
end cli ;
