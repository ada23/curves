with Interfaces.C.Extensions ;
with Interfaces ; use Interfaces ;

with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_IO;
with Ada.Short_Short_Integer_Text_IO ; use Ada.Short_Short_Integer_Text_Io ;
with Ada.Short_Integer_Text_Io; use Ada.Short_Integer_Text_Io ;

with Ada.Float_Text_Io; use Ada.Float_Text_Io ;
with Ada.Strings.Fixed ;
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded;
with Ada.Direct_IO ;

with hex ; 
with messages_h ;
--with datalog_defs ;
--with pss_defs ;

with alarm_h ;
with alarm_defs_h ;

package body binlog is

   function Open( files : cli.Args_Pkg.Vector ) return BinaryLogStream_Ptr is
     result : BinaryLogStream_Ptr := new BinaryLogStream ;
   begin
      result.files := files ;
      result.currentFile := cli.Args_Pkg.First(result.files) ;
      --binrec_io.Open( result.binfile , binrec_io.In_File , To_String(cli.Args_Pkg.Element(result.currentFile)));
      Put("Opening "); Put(To_String(cli.Args_Pkg.Element(result.currentFile))); New_Line ;
      Ada.Streams.Stream_Io.Open( result.binstream , Ada.Streams.Stream_Io.In_File ,  To_String(cli.Args_Pkg.Element(result.currentFile)));
      return result ;
   end Open ;
   
   function End_Of_File( stream : BinaryLogStream_Ptr ) return boolean is
      use cli.Args_Pkg ;
      use Ada.Streams.Stream_Io;
   begin
      if End_Of_File( stream.binstream ) 
      then
         Close(stream.binstream) ;
         stream.currentFile := cli.Args_Pkg.Next(stream.currentFile) ;
         if Ada.Strings.Unbounded.Length(Element(stream.currentFile)) = 0
         then   
            Put_Line("No more files");
            return true ;
         end if;
         if stream.currentFile = cli.Args_Pkg.No_Element
         then
            Put_Line("No more files");
            return true ;
         end if ;
         Put("Opening "); Put(To_String(cli.Args_Pkg.Element(stream.currentFile))); New_Line ;
         Open( stream.binstream , In_File , To_String(cli.Args_Pkg.Element(stream.currentFile))) ;
        -- return End_Of_File( stream ) ;
      end if ;
      return false ;
   end End_Of_FIle ;
   
   
   procedure Close( stream : in out BinaryLogStream_Ptr ) is
      use Ada.Streams.Stream_Io;
   begin
      if Is_Open( stream.binstream )
      then
         Close(stream.binstream) ;
      end if ;
      stream.currentFile := cli.Args_Pkg.No_Element ;
   end Close ;
  
   function Read( stream : in out BinaryLogStream_Ptr ) return messages_h.mm_queue_msg is

      result : messages_h.mm_queue_msg ;
      resultbytes : Ada.Streams.Stream_ELement_Array(1..64) ;    
      for resultbytes'address use result'address ;
      resultlen : Ada.Streams.Stream_Element_Offset ;
   begin
      -- binrec_io.Read(stream.binfile,result);
      Ada.Streams.Stream_IO.Read(stream.binstream,resultbytes,resultlen);
      --Put("Size "); Put(Integer(resultlen)); New_Line ;
      return result ;
   end Read ;
   
end binlog;
