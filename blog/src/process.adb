with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Ada.Short_Integer_Text_IO ; use Ada.Short_Integer_Text_IO ;
with Ada.Short_Short_Integer_Text_IO ; use Ada.Short_Short_Integer_Text_IO ;
with Ada.Calendar ;
with Ada.Calendar.Formatting ;
with Ada.Calendar.Conversions ;

with Interfaces; use INterfaces;
with Interfaces.C ;
with Interfaces.C.Extensions ; 
with hex ;

with messages_h ;
with binlog ;

with process.alarm ;
with process.playback ;
with process.thermal ;

package body process is

   
   procedure Initialize is
   begin
      null ;
   end Initialize ;
   
   function Create(name : string) return Processor'Class is
   begin
      if name = "alarm"
      then
         declare
            obj : process.alarm.alarmProcessor;
         begin
            return obj ;
         end ;
      elsif name = "thermal"
      then
         declare
            obj : process.thermal.ThermalProcessor ;
         begin
            return obj ;
         end ;
      elsif name = "playback"
      then
         declare
            obj : process.playback.playbackProcessor ;
         begin
            return obj ;
         end ;
      else
         raise Program_Error ;
      end if ;
   end Create ;
   
      
   procedure Exec( args : Args_Pkg.Vector ; p : in out Processor'Class ) is
      bf : binlog.BinaryLogStream_Ptr ;
      msg : messages_h.mm_queue_msg ;
   begin
      bf := binlog.Open(args) ;
      while Not binlog.End_Of_File(bf)
      loop
         msg := binlog.Read(bf) ;
         Exec(p , msg);
      end loop ;
      binlog.Close(bf) ;
      Finalize(p);
   end Exec ;
   
   procedure ShowHeader( msg : messages_h.mm_queue_msg ) is
      use Interfaces ;
   begin
      Put( messages_h.BoardName(Integer(msg.parent.sys_check.hdr.src)) ) ; Put(" : ");
      Put( messages_h.BoardName(Integer(msg.parent.sys_check.hdr.dst)) ) ; Put(" : ");      
      --  Put( hex.Image(Unsigned_8(msg.parent.sys_check.hdr.src)) ); Put( " : ");
      --  Put( hex.Image(Unsigned_8(msg.parent.sys_check.hdr.dst)) ); Put(" ");
      -- Put( hex.Image(Unsigned_16(msg.parent.sys_check.hdr.msg_field.msg_type) and 16#7f# )) ; Put(" ");
      Put( hex.Image(Unsigned_16(msg.parent.sys_check.hdr.msg_field.msg_type))) ; Put(" ");
   end ShowHeader ;
   
   procedure Show_DataLog_Timestamp (msg : messages_h.mm_queue_msg) is
         use Interfaces.C ;
      t : Ada.Calendar.Time ;
      use Interfaces.C.Extensions;
   begin
      t := Ada.Calendar.Conversions.To_Ada_Time( 84 + int(msg.parent.dlog.event_date_time.year ),
                                                 int(msg.parent.dlog.event_date_time.month - 1),
                                                 int(msg.parent.dlog.event_date_time.day ),
                                                 int(msg.parent.dlog.event_date_time.hour ),
                                                 int(msg.parent.dlog.event_date_time.minute ),
                                                 int(msg.parent.dlog.event_date_time.second ),
                                                    0);
      Put( Ada.Calendar.Formatting.Image(t) ) ; Put(".");
      Put( Short_Integer(msg.parent.dlog.millisecond) ) ;  Put(" ");
      New_LIne;
   end Show_DataLog_Timestamp ;
   procedure Show_DataLog_TimeStamp1 (msg : messages_h.mm_queue_msg) is
   begin 
      Put( 1984 + Short_Integer(msg.parent.dlog.event_date_time.year ));  Put("-");                             
      Put( Short_Short_Integer(msg.parent.dlog.event_date_time.month )) ; Put("-");
      Put( Short_Short_INteger(msg.parent.dlog.event_date_time.day ));  Put(" ");
      
      Put( Short_Short_Integer(msg.parent.dlog.event_date_time.hour ));  Put(":");
      Put( Short_Short_Integer(msg.parent.dlog.event_date_time.minute ));  Put(":");
      Put( Short_Short_Integer(msg.parent.dlog.event_date_time.second ));  Put(".");
      Put( Short_Integer(msg.parent.dlog.millisecond) ) ;  Put(" ");
      New_Line ;
   end Show_DataLog_TimeStamp1 ;
   
end process;
