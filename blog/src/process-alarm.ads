package process.alarm is

   type AlarmProcessor is new Processor with null record ;
   
   procedure Exec( p : in out AlarmProcessor ; msg : messages_h.mm_queue_msg ) ;
   procedure Finalize( p : in out AlarmProcessor ) ;
   
end process.alarm;
