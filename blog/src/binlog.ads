with Interfaces ; use Interfaces ;
with Ada.Direct_IO ;
with Ada.Streams.Stream_Io ;

with cli ;
with messages_h ;

package binlog is
   verbosity : Integer := 0 ;
   -- Message_Id : Unsigned_16 := 0 ;

   package binrec_io is new Ada.Direct_IO ( messages_h.mm_queue_msg );

   type BinaryLogStream is
      record
         binstream : Ada.Streams.Stream_Io.File_Type ;
         files : cli.Args_Pkg.Vector ;
         currentFile : cli.Args_Pkg.Cursor ;
      end record;
   type BinaryLogStream_Ptr is access BinaryLogStream ;
   function Open( files : cli.Args_Pkg.Vector ) return BinaryLogStream_Ptr ;
   function End_Of_File( stream : BinaryLogStream_Ptr ) return boolean ;
   procedure Close( stream : in out BinaryLogStream_Ptr );
   function Read( stream : in out BinaryLogStream_Ptr ) return messages_h.mm_queue_msg ;
   --
   --  procedure Iterate
   --    (Files : BinaryLogStream_Ptr ;
   --     Process   : not null access procedure (rec : access messages_h.mm_queue_msg));
   --
   --  procedure Iterate
   --    (Files : BinaryLogStream_Ptr ;
   --     message_type : Interfaces.Unsigned_16 ;
   --     Process   : not null access procedure (rec : access messages_h.mm_queue_msg));

end binlog;
