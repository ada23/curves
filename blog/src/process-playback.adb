with Text_Io; use Text_Io;
with Ada.Integer_Text_IO ; use Ada.Integer_Text_IO ;
with Ada.Strings.Fixed ;
with Ada.Strings.Unbounded ;

with GNAT.Source_Info ;

with cli ;

package body process.playback is

   procedure Initialize is
   begin
      null ;
   end Initialize ;
   
   procedure Exec( p : in out PlaybackProcessor ; msg : messages_h.mm_queue_msg ) is
      use Ada.Strings.Unbounded ;
      UDPOption : boolean := cli.UDPOption ;
      target : string := cli.target.all ;
      sep : natural ;
      port : Integer ;
      ip : Ada.Strings.Unbounded.unbounded_string ;
   begin
      -- Put_Line("Process.Playback.Exec");
      Put_Line( GNAT.Source_Info.Enclosing_Entity );
      sep := Ada.Strings.Fixed.Index(target,"@");
      if sep > 0
      then
         port := Integer'Value( target(1..sep-1) ) ;
         ip := To_Unbounded_String( target(sep+1..target'last)) ;
         Put("Port "); Put(Port) ; Put(" IP Address "); Put(To_String(ip)); New_Line ;
      else
         Put_Line("Target should be of the form port@ip");
         return ;
      end if ;
   end Exec ;
   procedure Finalize( p : in out PlaybackProcessor ) is
   begin
      null ;
   end Finalize ;
end process.playback;
