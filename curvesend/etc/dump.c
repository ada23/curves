#include <stdio.h>

void dump(unsigned char *ptr, int size)
{
    for (int i=0; i<size; ++i)
    {
        printf("%02x", ptr[i]) ;
    }
    printf("\n");
}
