pragma Ada_2012;
pragma Style_Checks (Off);

with Interfaces ;
with Interfaces.C; use Interfaces.C;
-- with stdint_h;
with Interfaces.C.Extensions;

package messages_h is

   SYS_CHECK_POST_REQUEST : constant := 16#0001#;  --  messages.h:27
   SYS_CHECK_POST_RESPONSE : constant := 16#0002#;  --  messages.h:28
   SYS_CHECK_RUN_REQUEST : constant := 16#0003#;  --  messages.h:29
   SYS_CHECK_ECHO_REQUEST : constant := 16#0004#;  --  messages.h:30
   SYS_CHECK_ECHO_RESPONSE : constant := 16#0005#;  --  messages.h:31
   SYS_CHECK_BOARD_ECHO_REQUEST : constant := 16#0006#;  --  messages.h:32
   SYS_CHECK_BOARD_ECHO_RESPONSE : constant := 16#0007#;  --  messages.h:33

   DATA_LOG_LOCAL_DELETE_ONE_LOG : constant := 16#0100#;  --  messages.h:35
   DATA_LOG_LOCAL_DISPLAY : constant := 16#0101#;  --  messages.h:36
   DATA_LOG_NEW_EVENT : constant := 16#0102#;  --  messages.h:37
   DATA_LOG_SS_SPECIFIC_EVENT : constant := 16#0103#;  --  messages.h:38
   DATA_LOG_LOCAL_DELETE_ALL_LOGS : constant := 16#0104#;  --  messages.h:39

   DATA_LOG_DATE_TIME_REFERENCE : constant := 16#0105# ;         -- New Addition
   DATA_LOG_BINARY_FILE_VERSION  : constant := 16#0106# ;        -- New Addition
   DATA_LOG_BINARY_FILE_MESSAGE_COUNT : constant := 16#0107# ;   -- New Addition
   DATA_LOG_CONSOLE_SBC_VER : constant := 16#0108# ;             -- New Addition
   DATA_LOG_CONSOLE_CONSOLE_ID : constant := 16#0109# ;          -- New Addition

   
   SENSOR_DATA_REQUEST : constant := 16#0200#;  --  messages.h:41
   SENSOR_DATA_RESPONSE : constant := 16#0201#;  --  messages.h:42
   SENSOR_DATA_PUSH : constant := 16#0202#;  --  messages.h:43

   FAN_DRIVE_AND_SENSE_SET_REQUEST : constant := 16#0300#;  --  messages.h:45
   FAN_DRIVE_AND_SENSE_SET_SUCCESS : constant := 16#0301#;  --  messages.h:46
   FAN_DRIVE_AND_SENSE_SET_FAILURE : constant := 16#0302#;  --  messages.h:47
   FAN_DRIVE_AND_SENSE_GET_REQUEST : constant := 16#0303#;  --  messages.h:48
   FAN_DRIVE_AND_SENSE_GET_SUCCESS : constant := 16#0304#;  --  messages.h:49
   FAN_DRIVE_AND_SENSE_GET_FAILURE : constant := 16#0305#;  --  messages.h:50

   BG_BOARD_QUANTUM_MEASUREMENT : constant := 16#0500#;  --  messages.h:52
   BG_BOARD_SWEEP_FLOW : constant := 16#0501#;  --  messages.h:53
   BG_BOARD_CALIBRATE : constant := 16#0502#;  --  messages.h:54
   BG_BOARD_GAS_VALUES : constant := 16#0503#;  --  messages.h:55
   BG_BOARD_PRESSURE_VALUES : constant := 16#0504#;  --  messages.h:56
   BG_BOARD_SONOFLOW_SENSOR : constant := 16#0505#;  --  messages.h:57
   BG_BOARD_SPECTRUM_GAS_VALUES : constant := 16#0506#;  --  messages.h:58
   BG_BOARD_PRESSURE_POINT : constant := 16#0507#;  --  messages.h:59
   BG_BOARD_GAS_VALUE_REQUEST : constant := 16#0508#;  --  messages.h:60
   BG_BOARD_GAS_VALUE_RESPONSE : constant := 16#0509#;  --  messages.h:61

   MC_BOARD_LIGHT_MSG : constant := 16#0600#;  --  messages.h:63
   MC_BOARD_LIGHT_RESPONSE : constant := 16#0601#;  --  messages.h:64
   MC_BOARD_BATTERY_VALUE_GET_REQUEST : constant := 16#0602#;  --  messages.h:65
   MC_BOARD_BATTERY_VALUE_GET_SUCCESS : constant := 16#0603#;  --  messages.h:66
   MC_BOARD_MAIN_POWER_DISCONNECTED : constant := 16#0604#;  --  messages.h:67

   ALARM_SET_REQUEST : constant := 16#0700#;  --  messages.h:69
   ALARM_SET_SUCCESS : constant := 16#0701#;  --  messages.h:70
   ALARM_SET_FAILURE : constant := 16#0702#;  --  messages.h:71
   ALARM_GET_REQUEST : constant := 16#0703#;  --  messages.h:72
   ALARM_GET_SUCCESS : constant := 16#0704#;  --  messages.h:73
   ALARM_GET_FAILURE : constant := 16#0705#;  --  messages.h:74

   AUDIO_SET_REQUEST : constant := 16#0800#;  --  messages.h:76
   AUDIO_SET_SUCCESS : constant := 16#0801#;  --  messages.h:77
   AUDIO_SET_FAILURE : constant := 16#0802#;  --  messages.h:78
   AUDIO_GET_REQUEST : constant := 16#0803#;  --  messages.h:79
   AUDIO_GET_SUCCESS : constant := 16#0804#;  --  messages.h:80
   AUDIO_GET_FAILURE : constant := 16#0805#;  --  messages.h:81

   VISUAL_SET_REQUEST : constant := 16#0900#;  --  messages.h:83
   VISUAL_SET_SUCCESS : constant := 16#0901#;  --  messages.h:84
   VISUAL_SET_FAILURE : constant := 16#0902#;  --  messages.h:85
   VISUAL_GET_REQUEST : constant := 16#0903#;  --  messages.h:86
   VISUAL_GET_SUCCESS : constant := 16#0904#;  --  messages.h:87
   VISUAL_GET_FAILURE : constant := 16#0905#;  --  messages.h:88

   PSS_PUMP_SET_REQUEST : constant := 16#1000#;  --  messages.h:90
   PSS_PUMP_SET_SUCCESS : constant := 16#1001#;  --  messages.h:91
   PSS_PUMP_SET_FAILURE : constant := 16#1002#;  --  messages.h:92
   PSS_PUMP_GET_REQUEST : constant := 16#1003#;  --  messages.h:93
   PSS_PUMP_GET_SUCCESS : constant := 16#1004#;  --  messages.h:94
   PSS_PUMP_GET_FAILURE : constant := 16#1005#;  --  messages.h:95

   PSS_POD_PRESSURE_SET_REQUEST : constant := 16#1100#;  --  messages.h:97
   PSS_POD_PRESSURE_SET_SUCCESS : constant := 16#1101#;  --  messages.h:98
   PSS_POD_PRESSURE_SET_FAILURE : constant := 16#1102#;  --  messages.h:99
   PSS_POD_PRESSURE_GET_REQUEST : constant := 16#1103#;  --  messages.h:100
   PSS_POD_PRESSURE_GET_SUCCESS : constant := 16#1104#;  --  messages.h:101
   PSS_POD_PRESSURE_GET_FAILURE : constant := 16#1105#;  --  messages.h:102

   PSS_PINCH_VALVE_SET_REQUEST : constant := 16#1200#;  --  messages.h:104
   PSS_PINCH_VALVE_SET_SUCCESS : constant := 16#1201#;  --  messages.h:105
   PSS_PINCH_VALVE_SET_FAILURE : constant := 16#1202#;  --  messages.h:106
   PSS_PINCH_VALVE_GET_REQUEST : constant := 16#1203#;  --  messages.h:107
   PSS_PINCH_VALVE_GET_SUCCESS : constant := 16#1204#;  --  messages.h:108
   PSS_PINCH_VALVE_GET_FAILURE : constant := 16#1205#;  --  messages.h:109

   PSS_SONOFLOW_GET_REQUEST : constant := 16#1300#;  --  messages.h:111
   PSS_SONOFLOW_GET_SUCCESS : constant := 16#1301#;  --  messages.h:112
   PSS_SONOFLOW_GET_FAILURE : constant := 16#1302#;  --  messages.h:113
   PSS_SONOFLOW_SET_REQUEST : constant := 16#1303#;  --  messages.h:114
   PSS_SONOFLOW_SET_SUCCESS : constant := 16#1304#;  --  messages.h:115
   PSS_SONOFLOW_SET_FAILURE : constant := 16#1305#;  --  messages.h:116

   PSS_POD_CANNULA_BMS_GET_REQUEST : constant := 16#1400#;  --  messages.h:118
   PSS_POD_CANNULA_BMS_GET_SUCCESS : constant := 16#1401#;  --  messages.h:119
   PSS_POD_CANNULA_BMS_GET_FAILURE : constant := 16#1402#;  --  messages.h:120
   PSS_POD_CANNULA_BMS_SET_REQUEST : constant := 16#1403#;  --  messages.h:121
   PSS_POD_CANNULA_BMS_SET_SUCCESS : constant := 16#1404#;  --  messages.h:122
   PSS_POD_CANNULA_BMS_SET_FAILURE : constant := 16#1405#;  --  messages.h:123

   PSS_POD_WASTE_BMS_GET_REQUEST : constant := 16#1500#;  --  messages.h:125
   PSS_POD_WASTE_BMS_GET_SUCCESS : constant := 16#1501#;  --  messages.h:126
   PSS_POD_WASTE_BMS_GET_FAILURE : constant := 16#1502#;  --  messages.h:127
   PSS_POD_WASTE_BMS_SET_REQUEST : constant := 16#1503#;  --  messages.h:128
   PSS_POD_WASTE_BMS_SET_SUCCESS : constant := 16#1504#;  --  messages.h:129
   PSS_POD_WASTE_BMS_SET_FAILURE : constant := 16#1505#;  --  messages.h:130

   PSS_DOOR_GET_REQUEST : constant := 16#1600#;  --  messages.h:132
   PSS_DOOR_GET_SUCCESS : constant := 16#1601#;  --  messages.h:133
   PSS_DOOR_GET_FAILURE : constant := 16#1602#;  --  messages.h:134
   PSS_DOOR_STATUS_UPDATE_INDICATION : constant := 16#1603#;  --  messages.h:135
   PSS_DOOR_SET_REQUEST : constant := 16#1604#;  --  messages.h:136
   PSS_DOOR_SET_SUCCESS : constant := 16#1605#;  --  messages.h:137
   PSS_DOOR_SET_FAILURE : constant := 16#1606#;  --  messages.h:138

   PSS_BAG_GET_REQUEST : constant := 16#1700#;  --  messages.h:140
   PSS_BAG_GET_SUCCESS : constant := 16#1701#;  --  messages.h:141
   PSS_BAG_GET_FAILURE : constant := 16#1702#;  --  messages.h:142
   PSS_BAG_SET_REQUEST : constant := 16#1703#;  --  messages.h:143
   PSS_BAG_SET_SUCCESS : constant := 16#1704#;  --  messages.h:144
   PSS_BAG_SET_FAILURE : constant := 16#1705#;  --  messages.h:145
   PSS_BAG_WEIGHT_REQUEST : constant := 16#1706#;  --  messages.h:146
   PSS_BAG_WEIGHT_SUCCESS : constant := 16#1707#;  --  messages.h:147
   PSS_BAG_WEIGHT_FAILURE : constant := 16#1708#;  --  messages.h:148

   PSS_UV_DISINFECTOR_SET_REQUEST : constant := 16#1800#;  --  messages.h:150
   PSS_UV_DISINFECTOR_SET_SUCCESS : constant := 16#1801#;  --  messages.h:151
   PSS_UV_DISINFECTOR_SET_FAILURE : constant := 16#1802#;  --  messages.h:152
   PSS_UV_DISINFECTOR_GET_REQUEST : constant := 16#1803#;  --  messages.h:153
   PSS_UV_DISINFECTOR_GET_SUCCESS : constant := 16#1804#;  --  messages.h:154
   PSS_UV_DISINFECTOR_GET_FAILURE : constant := 16#1805#;  --  messages.h:155

   PSS_DRAWER_LED_SET_REQUEST : constant := 16#1900#;  --  messages.h:157
   PSS_DRAWER_LED_SET_SUCCESS : constant := 16#1901#;  --  messages.h:158
   PSS_DRAWER_LED_SET_FAILURE : constant := 16#1902#;  --  messages.h:159
   PSS_DRAWER_LED_GET_REQUEST : constant := 16#1903#;  --  messages.h:160
   PSS_DRAWER_LED_GET_SUCCESS : constant := 16#1904#;  --  messages.h:161
   PSS_DRAWER_LED_GET_FAILURE : constant := 16#1905#;  --  messages.h:162

   TH_HEATER_SET_REQUEST : constant := 16#2000#;  --  messages.h:164
   TH_HEATER_SET_SUCCESS : constant := 16#2001#;  --  messages.h:165
   TH_HEATER_SET_FAILURE : constant := 16#2002#;  --  messages.h:166
   TH_HEATER_GET_REQUEST : constant := 16#2003#;  --  messages.h:167
   TH_HEATER_GET_SUCCESS : constant := 16#2004#;  --  messages.h:168
   TH_HEATER_GET_FAILURE : constant := 16#2005#;  --  messages.h:169

   TH_POD_TEMPERATURE_SET_REQUEST : constant := 16#2100#;  --  messages.h:171
   TH_POD_TEMPERATURE_SET_SUCCESS : constant := 16#2101#;  --  messages.h:172
   TH_POD_TEMPERATURE_SET_FAILURE : constant := 16#2102#;  --  messages.h:173
   TH_POD_TEMPERATURE_GET_REQUEST : constant := 16#2103#;  --  messages.h:174
   TH_POD_TEMPERATURE_GET_SUCCESS : constant := 16#2104#;  --  messages.h:175
   TH_POD_TEMPERATURE_GET_FAILURE : constant := 16#2105#;  --  messages.h:176

   TH_POD_CANOPY_TEMPERATURE_SET_REQUEST : constant := 16#2200#;  --  messages.h:178
   TH_POD_CANOPY_TEMPERATURE_SET_SUCCESS : constant := 16#2201#;  --  messages.h:179
   TH_POD_CANOPY_TEMPERATURE_SET_FAILURE : constant := 16#2202#;  --  messages.h:180
   TH_POD_CANOPY_TEMPERATURE_GET_REQUEST : constant := 16#2203#;  --  messages.h:181
   TH_POD_CANOPY_TEMPERATURE_GET_SUCCESS : constant := 16#2204#;  --  messages.h:182
   TH_POD_CANOPY_TEMPERATURE_GET_FAILURE : constant := 16#2205#;  --  messages.h:183

   TH_BLOOD_CANOPY_TEMPERATURE_SET_REQUEST : constant := 16#2300#;  --  messages.h:185
   TH_BLOOD_CANOPY_TEMPERATURE_SET_SUCCESS : constant := 16#2301#;  --  messages.h:186
   TH_BLOOD_CANOPY_TEMPERATURE_SET_FAILURE : constant := 16#2302#;  --  messages.h:187
   TH_BLOOD_CANOPY_TEMPERATURE_GET_REQUEST : constant := 16#2303#;  --  messages.h:188
   TH_BLOOD_CANOPY_TEMPERATURE_GET_SUCCESS : constant := 16#2304#;  --  messages.h:189
   TH_BLOOD_CANOPY_TEMPERATURE_GET_FAILURE : constant := 16#2305#;  --  messages.h:190

   TH_POD_CANOPY_FAN_SET_REQUEST : constant := 16#2400#;  --  messages.h:192
   TH_POD_CANOPY_FAN_SET_SUCCESS : constant := 16#2401#;  --  messages.h:193
   TH_POD_CANOPY_FAN_SET_FAILURE : constant := 16#2402#;  --  messages.h:194
   TH_POD_CANOPY_FAN_GET_REQUEST : constant := 16#2403#;  --  messages.h:195
   TH_POD_CANOPY_FAN_GET_SUCCESS : constant := 16#2404#;  --  messages.h:196
   TH_POD_CANOPY_FAN_GET_FAILURE : constant := 16#2405#;  --  messages.h:197

   TH_BLOOD_CANOPY_FAN_SET_REQUEST : constant := 16#2500#;  --  messages.h:199
   TH_BLOOD_CANOPY_FAN_SET_SUCCESS : constant := 16#2501#;  --  messages.h:200
   TH_BLOOD_CANOPY_FAN_SET_FAILURE : constant := 16#2502#;  --  messages.h:201
   TH_BLOOD_CANOPY_FAN_GET_REQUEST : constant := 16#2503#;  --  messages.h:202
   TH_BLOOD_CANOPY_FAN_GET_SUCCESS : constant := 16#2504#;  --  messages.h:203
   TH_BLOOD_CANOPY_FAN_GET_FAILURE : constant := 16#2505#;  --  messages.h:204

   SYSTEM_DATE_TIME_SET_REQUEST : constant := 16#3000#;  --  messages.h:206
   SYSTEM_DATE_TIME_SET_SUCCESS : constant := 16#3001#;  --  messages.h:207
   SYSTEM_DATE_TIME_SET_FAILURE : constant := 16#3002#;  --  messages.h:208
   SYSTEM_END_OF_TREATMENT_SET_REQUEST : constant := 16#3003#;  --  messages.h:209
   SYSTEM_END_OF_TREATMENT_SET_SUCCESS : constant := 16#3004#;  --  messages.h:210
   SYSTEM_END_OF_TREATMENT_SET_FAILURE : constant := 16#3005#;  --  messages.h:211
   SYSTEM_SHUTDOWN_SET_REQUEST : constant := 16#3006#;  --  messages.h:212
   SYSTEM_SHUTDOWN_SET_SUCCESS : constant := 16#3007#;  --  messages.h:213
   SYSTEM_SHUTDOWN_SET_FAILURE : constant := 16#3008#;  --  messages.h:214

   SBC_START_DISPLAY_SET_REQUEST : constant := 16#3100#;  --  messages.h:216
   SBC_START_DISPLAY_SET_SUCCESS : constant := 16#3101#;  --  messages.h:217
   SBC_START_DISPLAY_SET_FAILURE : constant := 16#3102#;  --  messages.h:218
   SBC_BABY_WEIGHT_SET_REQUEST : constant := 16#3103#;  --  messages.h:219
   SBC_BABY_WEIGHT_SET_SUCCESS : constant := 16#3104#;  --  messages.h:220
   SBC_BABY_WEIGHT_SET_FAILURE : constant := 16#3105#;  --  messages.h:221
   SBC_BABY_WEIGHT_GET_REQUEST : constant := 16#3106#;  --  messages.h:222
   SBC_BABY_WEIGHT_GET_SUCCESS : constant := 16#3107#;  --  messages.h:223
   SBC_BABY_WEIGHT_GET_FAILURE : constant := 16#3108#;  --  messages.h:224

   MM_MSG_TYPE_LAST : constant := 16#FFFF#;  --  messages.h:226

   DL_SEQ_NUM_STRING_SIZE : constant := 8;  --  messages.h:230
   DL_DATE_STRING_SIZE : constant := 11;  --  messages.h:231
   DL_TIME_STRING_SIZE : constant := 12;  --  messages.h:232
   DL_BOARD_ID_STRING_MAX : constant := 18;  --  messages.h:233
   DL_SS_ID_STRING_MAX : constant := 21;  --  messages.h:234
   DL_EVENT_MAJOR_STRING_MAX : constant := 21;  --  messages.h:235
   DL_EVENT_MINOR_STRING_MAX : constant := 21;  --  messages.h:236
   DL_VAL_STRING_SIZE : constant := 10;  --  messages.h:237
   DL_NUM_32BIT_VAL : constant := 10;  --  messages.h:238
   --  unsupported macro: DL_NUM_64BIT_VAL (DL_NUM_32BIT_VAL / 2)
   --  unsupported macro: DL_STRING_SIZE (DL_NUM_32BIT_VAL * sizeof(uint32_t))

   DL_BAD_CRC_STRING_SIZE : constant := 8;  --  messages.h:241
   DL_FIELD_SEP_CHARS : constant := 64;  --  messages.h:242
   --  unsupported macro: DL_HEADER_SIZE (DL_SEQ_NUM_STRING_SIZE + (DL_DATE_STRING_SIZE * 2) + (DL_TIME_STRING_SIZE * 2) + DL_BOARD_ID_STRING_MAX)
   --  unsupported macro: DL_TRAILER_SIZE (DL_SS_ID_STRING_MAX + DL_EVENT_MAJOR_STRING_MAX + DL_EVENT_MINOR_STRING_MAX + (DL_NUM_32BIT_VAL * DL_VAL_STRING_SIZE) + DL_BAD_CRC_STRING_SIZE)
   --  unsupported macro: DL_RECORD_MAX_NUM_BYTES (DL_HEADER_SIZE + DL_TRAILER_SIZE + DL_FIELD_SEP_CHARS)

   DL_SS_SYS_CHECK : constant := 0;  --  messages.h:249
   DL_SS_SENSOR_UPDATE : constant := 1;  --  messages.h:250
   DL_SS_PRESSURE_MANAGER_UPDATE : constant := 2;  --  messages.h:251
   DL_SS_POD_MANAGER_UPDATE : constant := 3;  --  messages.h:252
   DL_SS_POD_CANOPY_MANAGER_UPDATE : constant := 4;  --  messages.h:253
   DL_SS_BLOOD_CANOPY_MANAGER_UPDATE : constant := 5;  --  messages.h:254
   DL_SS_HEATER_MANAGER_UPDATE : constant := 6;  --  messages.h:255
   DL_SS_GASMIX_UPDATE : constant := 7;  --  messages.h:256
   DL_SS_SYSTEM_STATE_MACHINE : constant := 8;  --  messages.h:257
   DL_SS_PINCH_VALVE_MANAGER_UPDATE : constant := 9;  --  messages.h:258
   DL_SS_SONOFLOW_SENSOR_MANAGER_UPDATE : constant := 10;  --  messages.h:259
   DL_SS_POD_CANNULA_BMS_MANAGER_UPDATE : constant := 11;  --  messages.h:260
   DL_SS_POD_WASTE_BMS_MANAGER_UPDATE : constant := 12;  --  messages.h:261
   DL_SS_DOOR_MANAGER_UPDATE : constant := 13;  --  messages.h:262
   DL_SS_BAG_MANAGER_UPDATE : constant := 14;  --  messages.h:263
   DL_SS_BLOOD_GAS_MANAGER_UPDATE : constant := 15;  --  messages.h:264
   DL_SS_UV_DISINFECTOR_MANAGER_UPDATE : constant := 16;  --  messages.h:265
   DL_SS_MM_IN_UPDATE : constant := 17;  --  messages.h:266
   DL_SS_BLOOD_MANAGER_UPDATE : constant := 18;  --  messages.h:267
   DL_SS_DRAWER_LED_MANAGER_UPDATE : constant := 19;  --  messages.h:268
   DL_SS_ALARM_MANAGER_UPDATE : constant := 20;  --  messages.h:269
   DL_SS_PUMP_MANAGER_UPDATE : constant := 21;  --  messages.h:270
   DL_SS_BATTERY_MANAGER_UPDATE : constant := 22;  --  messages.h:271
   DL_SS_CANOPY_POSITION_MANAGER_UPDATE : constant := 23;  --  messages.h:272
   DL_SS_BLOOD_CANOPY_FAN_MANAGER_UPDATE : constant := 24;  --  messages.h:273
   DL_SS_POD_CANOPY_FAN_MANAGER_UPDATE : constant := 25;  --  messages.h:274
   DL_SS_POWER_MANAGER_UPDATE : constant := 26;  --  messages.h:275
   DL_SS_WDT : constant := 27;  --  messages.h:276
   DL_SS_MAX_SUBSYSTEMS : constant := 28;  --  messages.h:277

   DL_EMAJ_SYS_CHECK_SYSTEM : constant := 0;  --  messages.h:281
   DL_EMAJ_SYS_CHECK_POST_START : constant := 1;  --  messages.h:282
   DL_EMAJ_SYS_CHECK_POST_SUCCESS : constant := 2;  --  messages.h:283
   DL_EMAJ_SYS_CHECK_POST_FAILURE : constant := 3;  --  messages.h:284
   DL_EMAJ_SYS_CHECK_THREAD_POST_START : constant := 4;  --  messages.h:285
   DL_EMAJ_SYS_CHECK_THREAD_POST_SUCCESS : constant := 5;  --  messages.h:286
   DL_EMAJ_SYS_CHECK_THREAD_POST_FAILURE : constant := 6;  --  messages.h:287
   DL_EMAJ_SYS_CHECK_HC_THREAD_TIMEOUT : constant := 7;  --  messages.h:288
   DL_EMAJ_SENSOR_UPDATE_ID : constant := 8;  --  messages.h:289
   DL_EMAJ_PRESSURE_CPID_UPDATE : constant := 9;  --  messages.h:290
   DL_EMAJ_POD_CPID_UPDATE : constant := 10;  --  messages.h:291
   DL_EMAJ_BLOCK_CPID_UPDATE : constant := 11;  --  messages.h:292
   DL_EMAJ_BLOCK1_CPID_UPDATE : constant := 12;  --  messages.h:293
   DL_EMAJ_BLOCK2_CPID_UPDATE : constant := 13;  --  messages.h:294
   DL_EMAJ_OUTLET_CPID_UPDATE : constant := 14;  --  messages.h:295
   DL_EMAJ_OUTLET1_CPID_UPDATE : constant := 15;  --  messages.h:296
   DL_EMAJ_OUTLET2_CPID_UPDATE : constant := 16;  --  messages.h:297
   DL_EMAJ_CANOPY_CPID_UPDATE : constant := 17;  --  messages.h:298
   DL_EMAJ_PSS_BLOCK1_UPDATE : constant := 18;  --  messages.h:299
   DL_EMAJ_PSS_BLOCK2_UPDATE : constant := 19;  --  messages.h:300
   DL_EMAJ_POD_CANOPY_B1_UPDATE : constant := 20;  --  messages.h:301
   DL_EMAJ_POD_CANOPY_B2_UPDATE : constant := 21;  --  messages.h:302
   DL_EMAJ_BLOOD_CANOPY_UPDATE : constant := 22;  --  messages.h:303
   DL_EMAJ_STATE_TRANS : constant := 23;  --  messages.h:304
   DL_EMAJ_ALICAT_SETPOINT_UPDATE : constant := 24;  --  messages.h:305
   DL_EMAJ_GAS_CALIBRATE_UPDATE : constant := 25;  --  messages.h:306
   DL_EMAJ_RUN_STATE_ENTERED : constant := 26;  --  messages.h:307
   DL_EMAJ_SOURCE_VALVE : constant := 27;  --  messages.h:308
   DL_EMAJ_WASTE_VALVE : constant := 28;  --  messages.h:309
   DL_EMAJ_BAG_SET_ACTIVE : constant := 29;  --  messages.h:310
   DL_EMAJ_FLOW_MEASURED : constant := 30;  --  messages.h:311
   DL_EMAJ_VALUES_READ : constant := 31;  --  messages.h:312
   DL_EMAJ_ERROR_READING_SENSOR : constant := 32;  --  messages.h:313
   DL_EMAJ_DOOR_STATE_UPDATE : constant := 33;  --  messages.h:314
   DL_EMAJ_PINCH_VALVE_REQUEST : constant := 34;  --  messages.h:315
   DL_EMAJ_ALL_BAG_SETS_UNAVAIL : constant := 35;  --  messages.h:316
   DL_EMAJ_BAG_SET_UNAVAIL : constant := 36;  --  messages.h:317
   DL_EMAJ_BAG_SET_SELECTION : constant := 37;  --  messages.h:318
   DL_EMAJ_BLOOD_GAS_UPDATE : constant := 38;  --  messages.h:319
   DL_EMAJ_UV1_OPERATION : constant := 39;  --  messages.h:320
   DL_EMAJ_UV2_OPERATION : constant := 40;  --  messages.h:321
   DL_EMAJ_SPI_DEV_0 : constant := 41;  --  messages.h:322
   DL_EMAJ_SPI_DEV_1 : constant := 42;  --  messages.h:323
   DL_EMAJ_GAS_SOLENOID_UPDATE : constant := 43;  --  messages.h:324
   DL_EMAJ_PRESSURE_CALIBRATION : constant := 44;  --  messages.h:325
   DL_EMAJ_MM_IN_CRC_IDLE : constant := 45;  --  messages.h:326
   DL_EMAJ_MM_IN_CRC_FRAME_ERROR : constant := 46;  --  messages.h:327
   DL_EMAJ_MM_IN_CRC_INVALID : constant := 47;  --  messages.h:328
   DL_EMAJ_SSMM_PS_READINESS : constant := 48;  --  messages.h:329
   DL_EMAJ_SSMM_TH_READINESS : constant := 49;  --  messages.h:330
   DL_EMAJ_SSMM_BG_READINESS : constant := 50;  --  messages.h:331
   DL_EMAJ_SSMM_START_DISPLAY_RECEIVED : constant := 51;  --  messages.h:332
   DL_EMAJ_SSMM_RUN_STATE : constant := 52;  --  messages.h:333
   DL_EMAJ_SYSTEM_SHUTDOWN_RECEIVED : constant := 53;  --  messages.h:334
   DL_EMAJ_ALARM_DECLARE : constant := 54;  --  messages.h:335
   DL_EMAJ_ALARM_QUIESCE : constant := 55;  --  messages.h:336
   DL_EMAJ_ALARM_CLEAR : constant := 56;  --  messages.h:337
   DL_EMAJ_BATTERY_VALUE : constant := 57;  --  messages.h:338
   DL_EMAJ_UV3_OPERATION : constant := 58;  --  messages.h:339
   DL_EMAJ_BLOOD_DOOR : constant := 59;  --  messages.h:340
   DL_EMAJ_POD_DOOR : constant := 60;  --  messages.h:341
   DL_EMAJ_FLUID_LEVEL_UPDATE : constant := 61;  --  messages.h:342
   DL_EMAJ_SYSTEM_END_OF_TREATMENT_RECEIVED : constant := 62;  --  messages.h:343
   DL_EMAJ_SYS_CHECK_HC_BOARD_TIMEOUT : constant := 63;  --  messages.h:344
   DL_EMAJ_POWER_SIG_2V_048A : constant := 64;  --  messages.h:345
   DL_EMAJ_POWER_SIG_2V_048P : constant := 65;  --  messages.h:346
   DL_EMAJ_POWER_SIG_3P3V : constant := 66;  --  messages.h:347
   DL_EMAJ_POWER_SIG_5V : constant := 67;  --  messages.h:348
   DL_EMAJ_POWER_SIG_5V_A : constant := 68;  --  messages.h:349
   DL_EMAJ_POWER_SIG_10V : constant := 69;  --  messages.h:350
   DL_EMAJ_POWER_SIG_24V : constant := 70;  --  messages.h:351
   DL_EMAJ_WDT_INIT : constant := 71;  --  messages.h:352
   DL_EMAJ_SSMM_READINESS_WAIT_STATE : constant := 72;  --  messages.h:353
   DL_EMAJ_SSMM_SBC_SYNC_STATE : constant := 73;  --  messages.h:354
   DL_EMAJ_STATE_TRANS_SM1 : constant := 74;  --  messages.h:355
   DL_EMAJ_STATE_TRANS_SM2 : constant := 75;  --  messages.h:356
   DL_EMAJ_BATTERY_MAIN_POWER : constant := 76;  --  messages.h:357
   DL_EMAJ_BATTERY_COMMUNICATION : constant := 77;  --  messages.h:358
   DL_EMAJ_MM_IN_MW_ML_ERROR : constant := 78;  --  messages.h:359
   DL_EMAJ_MM_IN_MW_DR_ERROR : constant := 79;  --  messages.h:360

   DL_EMIN_SYS_CHECK_STARTUP : constant := 0;  --  messages.h:365
   DL_EMIN_POST_RAM_TEST : constant := 1;  --  messages.h:366
   DL_EMIN_POST_FLASH_TEST : constant := 2;  --  messages.h:367
   DL_EMIN_POST_NAME : constant := 3;  --  messages.h:368
   DL_EMIN_POST_READY : constant := 4;  --  messages.h:369
   DL_EMIN_POST_DONE_WAIT : constant := 5;  --  messages.h:370
   DL_EMIN_RUN_READY : constant := 6;  --  messages.h:371
   DL_EMIN_RUN : constant := 7;  --  messages.h:372
   DL_EMIN_POST_READY_FAILED_THREAD_STATE : constant := 8;  --  messages.h:373
   DL_EMIN_POST_READY_FAILED_TXQ_SEND : constant := 9;  --  messages.h:374
   DL_EMIN_POST_DONE_WAIT_FAILED_INVALID_THREAD_ID_RX : constant := 10;  --  messages.h:375
   DL_EMIN_POST_DONE_WAIT_FAILED_MESSAGE_TIMEOUT : constant := 11;  --  messages.h:376
   DL_EMIN_POST_DONE_WAIT_FAILED_POST_NOT_SUCCESSFUL : constant := 12;  --  messages.h:377
   DL_EMIN_POST_DONE_WAIT_FAILED_UNEXPECTED_MESSAGE : constant := 13;  --  messages.h:378
   DL_EMIN_POST_DONE_WAIT_FAILED_TXQ_SEND : constant := 14;  --  messages.h:379
   DL_EMIN_POST_DONE_WAIT_FAILED_INVALID_THREAD_STATE : constant := 15;  --  messages.h:380
   DL_EMIN_RUN_FAILED_TXQ_SEND : constant := 16;  --  messages.h:381
   DL_EMIN_ECHO_REQUEST_FAILED : constant := 17;  --  messages.h:382
   DL_EMIN_HC_NO_RESP_FROM_THREAD : constant := 18;  --  messages.h:383
   DL_EMIN_SENSOR_UPDATE_REFINED : constant := 19;  --  messages.h:384
   DL_EMIN_PRESSUREM_VALUES : constant := 20;  --  messages.h:385
   DL_EMIN_TEMPERATUREM_BLOCK_VALUES : constant := 21;  --  messages.h:386
   DL_EMIN_TEMPERATUREM_POD_VALUES : constant := 22;  --  messages.h:387
   DL_EMIN_TEMPERATUREM_POD_CANOPY_VALUES : constant := 23;  --  messages.h:388
   DL_EMIN_TEMPERATUREM_BLOOD_CANOPY_VALUES : constant := 24;  --  messages.h:389
   DL_EMIN_HEATERM_VALUES : constant := 25;  --  messages.h:390
   DL_EMIN_CURRENT_AND_LAST_STATE : constant := 26;  --  messages.h:391
   DL_EMIN_ALICAT_VALUES : constant := 27;  --  messages.h:392
   DL_EMIN_GAS_CALIBRATE_VALUES : constant := 28;  --  messages.h:393
   DL_EMIN_READY : constant := 29;  --  messages.h:394
   DL_EMIN_SET_ON : constant := 30;  --  messages.h:395
   DL_EMIN_SET_OFF : constant := 31;  --  messages.h:396
   DL_EMIN_SET_NUMBER : constant := 32;  --  messages.h:397
   DL_EMIN_FLOW_UNITS : constant := 33;  --  messages.h:398
   DL_EMIN_BMS_MEASUREMENTS : constant := 34;  --  messages.h:399
   DL_EMIN_NO_VALUES_AVAILABLE : constant := 35;  --  messages.h:400
   DL_EMIN_DOOR_IS_OPENED : constant := 36;  --  messages.h:401
   DL_EMIN_DOOR_IS_CLOSED : constant := 37;  --  messages.h:402
   DL_EMIN_BAG_SET_SET_FAILURE : constant := 38;  --  messages.h:403
   DL_EMIN_SOURCE_AND_WASTE_PSS : constant := 39;  --  messages.h:404
   DL_EMIN_BLOOD_GAS_VALUES_SENT_TO_UI : constant := 40;  --  messages.h:405
   DL_EMIN_SWEEP_FLOW_VALUES_RECV_FROM_UI : constant := 41;  --  messages.h:406
   DL_EMIN_SWEEP_FLOW_VALUES_SENT_TO_UI : constant := 42;  --  messages.h:407
   DL_EMIN_ENABLED : constant := 43;  --  messages.h:408
   DL_EMIN_DISABLED : constant := 44;  --  messages.h:409
   DL_EMIN_ERROR_CODE : constant := 45;  --  messages.h:410
   DL_EMIN_ADI_GAS_PERCENTAGE : constant := 46;  --  messages.h:411
   DL_EMIN_MM_IN_SRC_MT : constant := 47;  --  messages.h:412
   DL_EMIN_MM_IN_SRC_MT_MSGCRC_CALCRC : constant := 48;  --  messages.h:413
   DL_EMIN_PRESSURE_CALIBRATION : constant := 49;  --  messages.h:414
   DL_EMIN_BLOOD_SPECTRUM_VALUES_SENT_TO_UI : constant := 50;  --  messages.h:415
   DL_EMIN_SSMM_IS_READY : constant := 51;  --  messages.h:416
   DL_EMIN_SSMM_IS_NOT_READY : constant := 52;  --  messages.h:417
   DL_EMIN_SSMM_START_DISPLAY_SUCCESS : constant := 53;  --  messages.h:418
   DL_EMIN_SSMM_START_DISPLAY_FAILURE : constant := 54;  --  messages.h:419
   DL_EMIN_SSMM_STATE_ENTERED : constant := 55;  --  messages.h:420
   DL_EMIN_SYSTEM_SHUTDOWN_RECEIVED : constant := 56;  --  messages.h:421
   DL_EMIN_ALARM_PROCESSED : constant := 57;  --  messages.h:422
   DL_EMIN_ALARM_NOT_PROCESSED : constant := 58;  --  messages.h:423
   DL_EMIN_BATTERY_STATUS_VALUES : constant := 59;  --  messages.h:424
   DL_EMIN_BLOOD_GAS_VALUES_SENT_TO_UI2 : constant := 60;  --  messages.h:425
   DL_EMIN_SIGH_CALI_STATUS : constant := 61;  --  messages.h:426
   DL_EMIN_BLOOD_GAS_PRESSURE_VALUES_TO_UI : constant := 62;  --  messages.h:427
   DL_EMIN_DOOR_OPENED : constant := 63;  --  messages.h:428
   DL_EMIN_DOOR_CLOSED : constant := 64;  --  messages.h:429
   DL_EMIN_DOOR_OPENED_ALARM : constant := 65;  --  messages.h:430
   DL_EMIN_LOW_POST_SAT_ALARM : constant := 66;  --  messages.h:431
   DL_EMIN_HIGH_TOTAL_SWEEP_FLOW : constant := 67;  --  messages.h:432
   DL_EMIN_SBC_BG_BABY_WEIGHT : constant := 68;  --  messages.h:433
   DL_EMIN_BG_FLOWVALUE_CHANGE_ALARM : constant := 69;  --  messages.h:434
   DL_EMIN_BATTERY_TIME_REMAINING : constant := 70;  --  messages.h:435
   DL_EMIN_O2_SETPOINT_TOO_HIGH : constant := 71;  --  messages.h:436
   DL_EMIN_O2_SETPOINT_TOO_LOW : constant := 72;  --  messages.h:437
   DL_EMIN_O2_PERCENT_SETPOINT_TOO_HIGH : constant := 73;  --  messages.h:438
   DL_EMIN_CO2_SETPOINT_TOO_HIGH : constant := 74;  --  messages.h:439
   DL_EMIN_CO2_SETPOINT_TOO_LOW : constant := 75;  --  messages.h:440
   DL_EMIN_N2_SETPOINT_TOO_HIGH : constant := 76;  --  messages.h:441
   DL_EMIN_N2_SETPOINT_TOO_LOW : constant := 77;  --  messages.h:442
   DL_EMIN_GASS_CALIBRATE_FAILED : constant := 78;  --  messages.h:443
   DL_EMIN_GAS_BLEND_MISMATCH : constant := 79;  --  messages.h:444
   DL_EMIN_SWEEP_FLOW_MISMATCH : constant := 80;  --  messages.h:445
   DL_EMIN_SOURCE_FLUID_LEVEL : constant := 81;  --  messages.h:446
   DL_EMIN_WASTE_FLUID_LEVEL : constant := 82;  --  messages.h:447
   DL_EMIN_SYSTEM_END_OF_TREATMENT_RECEIVED : constant := 83;  --  messages.h:448
   DL_EMIN_HC_NO_RESP_FROM_PS_BOARD : constant := 84;  --  messages.h:449
   DL_EMIN_HC_NO_RESP_FROM_TH_BOARD : constant := 85;  --  messages.h:450
   DL_EMIN_HC_NO_RESP_FROM_BG_BOARD : constant := 86;  --  messages.h:451
   DL_EMIN_HC_NO_RESP_FROM_CON_BOARD : constant := 87;  --  messages.h:452
   DL_EMIN_POWER_SIG_IS_GOOD : constant := 88;  --  messages.h:453
   DL_EMIN_POWER_SIG_IS_BAD : constant := 89;  --  messages.h:454
   DL_EMIN_POST_NETWORK_TEST : constant := 90;  --  messages.h:455
   DL_EMIN_WDT_NORMAL : constant := 91;  --  messages.h:456
   DL_EMIN_IWDT_CAUSED_RESET : constant := 92;  --  messages.h:457
   DL_EMIN_WDT_OPEN_ERROR : constant := 93;  --  messages.h:458
   DL_EMIN_WDT_REFRESH_ERROR : constant := 94;  --  messages.h:459
   DL_EMIN_BATTERY_DISCHARGING : constant := 95;  --  messages.h:460
   DL_EMIN_BATTERY_NOT_DISCHARGING : constant := 96;  --  messages.h:461
   DL_EMIN_BATTERY_RESPONDING : constant := 97;  --  messages.h:462
   DL_EMIN_BATTERY_NOT_RESPONDING : constant := 98;  --  messages.h:463
   DL_EMIN_GAS_BLEND_MATCH : constant := 99;  --  messages.h:464
   DL_EMIN_BLOOD_GAS_FIO2_FICO2_FEO2_FECO2 : constant := 100;  --  messages.h:465
   DL_EMIN_ARTERIAL_PRESSURE : constant := 101;  --  messages.h:466
   DL_EMIN_ALARM_PROCESSED_HIGH : constant := 102;  --  messages.h:467
   DL_EMIN_ALARM_PROCESSED_LOW_MED : constant := 103;  --  messages.h:468
   DL_EMIN_BG_FLOW_PARAMETERS : constant := 104;  --  messages.h:469
   DL_EMIN_MM_IN_MW_ML_SRC_MT_ML : constant := 105;  --  messages.h:470
   DL_EMIN_MM_IN_MW_DR_STATUS : constant := 106;  --  messages.h:471

   DL_NO_VALUE_FORMAT : constant := 0;  --  messages.h:475
   DL_DEC_VALUE_FORMAT : constant := 1;  --  messages.h:476
   DL_HEX_VALUE_FORMAT : constant := 2;  --  messages.h:477
   DL_DEC_DOUBLE_VALUE_FORMAT : constant := 3;  --  messages.h:478
   DL_STRING_FORMAT : constant := 4;  --  messages.h:479
   DL_STATE_CHANGE_FORMAT : constant := 5;  --  messages.h:480
   DL_ALICAT_METER_INFO_FORMAT : constant := 6;  --  messages.h:481
   DL_ALICAT_CONTROLLER_INFO_FORMAT : constant := 7;  --  messages.h:482
   DL_ALARM_INFO_FORMAT : constant := 8;  --  messages.h:483

   DL_NO_VALUES_SPECIFIED : constant := 0;  --  messages.h:487
   --  unsupported macro: DL_NO_VALUES_PTR (void *)0

   DL_SS_SPECIFIC_LOG_MAX : constant := 4;  --  messages.h:492

   DL_MESSAGE_TYPE_MASK : constant := 16#3FFF#;  --  messages.h:495

   MM_MSG_INT_QUEUE_XFER_ONLY_PAIRED : constant := 0;  --  messages.h:618
   MM_MSG_INT_QUEUE_XFER_ONLY_SYSTEM : constant := 1;  --  messages.h:619
   MM_MSG_THREAD_DIRECT : constant := 2;  --  messages.h:620

   MM_MSG_TYPE_REG_SUCCESS : constant := 0;  --  messages.h:623
   MM_MSG_TYPE_REG_NOT_INIT : constant := 1;  --  messages.h:624
   MM_MSG_TYPE_REG_FULL : constant := 2;  --  messages.h:625

   MM_MSG_MEM_GET_SUCCESS : constant := 0;  --  messages.h:628

   MM_MSG_MC_LINK : constant := 0;  --  messages.h:631
   MM_MSG_PS_LINK : constant := 1;  --  messages.h:632
   MM_MSG_TH_LINK : constant := 2;  --  messages.h:633
   MM_MSG_BG_LINK : constant := 3;  --  messages.h:634
   MM_MSG_CON_LINK : constant := 4;  --  messages.h:635
   MM_MSG_LINK_MAX : constant := 5;  --  messages.h:636
   MM_MSG_LINK_UNUSED : constant := 16#FFFE#;  --  messages.h:637
   MM_MSG_UNKNOWN_LINK : constant := 16#FFFF#;  --  messages.h:638

   MM_MC_BOARD_ID : constant := 16#30#;  --  messages.h:641
   MM_PS_BOARD_ID : constant := 16#31#;  --  messages.h:642
   MM_TH_BOARD_ID : constant := 16#32#;  --  messages.h:643
   MM_BG_BOARD_ID : constant := 16#33#;  --  messages.h:644
   MM_CON_BOARD_ID : constant := 16#34#;  --  messages.h:645

   MM_REA_OUI_MAC_H : constant := 16#00003055#;  --  messages.h:648
   --  unsupported macro: MM_USE_OUI_MAC_H MM_REA_OUI_MAC_H

   MM_MC_MAC_L : constant := 16#000076B8#;  --  messages.h:650
   MM_PS_MAC_L : constant := 16#000076C8#;  --  messages.h:651
   MM_TH_MAC_L : constant := 16#000076D8#;  --  messages.h:652
   MM_BG_MAC_L : constant := 16#000076E8#;  --  messages.h:653
   MM_UI_MAC_L : constant := 16#000076F8#;  --  messages.h:654
   --  unsupported macro: MM_MC_IP_ADDRESS IP_ADDRESS(192,168,0,2)
   --  unsupported macro: MM_PS_IP_ADDRESS IP_ADDRESS(192,168,0,4)
   --  unsupported macro: MM_TH_IP_ADDRESS IP_ADDRESS(192,168,0,6)
   --  unsupported macro: MM_BG_IP_ADDRESS IP_ADDRESS(192,168,0,8)
   --  unsupported macro: MM_UI_IP_ADDRESS IP_ADDRESS(192,168,0,10)

   MM_PS_TCP_PORT : constant := 1000;  --  messages.h:664
   MM_TH_TCP_PORT : constant := 1001;  --  messages.h:665
   MM_BG_TCP_PORT : constant := 1002;  --  messages.h:666
   MM_UI_TCP_PORT : constant := 2003;  --  messages.h:667

   SUBNET_MASK : constant := 16#FFFFFF00#;  --  messages.h:670

   MM_MSG_EXT_FREE_MEM_SUCCESS : constant := 0;  --  messages.h:749

   MAX_DL_NAME_LEN : constant := 34;  --  messages.h:752
   MIN_DL_NAME_LEN : constant := 9;  --  messages.h:753
   NAME_T1_CHAR_OFFSET : constant := 3;  --  messages.h:754
   NAME_X_CHAR_OFFSET : constant := 2;  --  messages.h:755
   NAME_T2_CHAR_OFFSET : constant := 1;  --  messages.h:756

   MM_MSG_PKT_SIZE_MAX : constant := 2048;  --  messages.h:759
   MM_MSG_THREADX_MAX_MESSAGE_SIZE : constant := 64;  --  messages.h:760
   MM_MSG_CRC_SIZE : constant := 2;  --  messages.h:761
   --  unsupported macro: MM_MSG_MESSAGE_SIZE (MM_MSG_THREADX_MAX_MESSAGE_SIZE - sizeof(mm_xfer_info))
   --  unsupported macro: MM_MSG_CRC_CALC_MESSAGE_SIZE (MM_MSG_MESSAGE_SIZE - MM_MSG_CRC_SIZE)
   --  unsupported macro: MM_MSG_APP_MESSAGE_SIZE (MM_MSG_MESSAGE_SIZE - (sizeof(mm_hdr) + MM_MSG_CRC_SIZE))

   MM_POLY : constant := 16#11021#;  --  messages.h:767

   MM_MSG_CRC_IDLE : constant := 0;  --  messages.h:770
   MM_MSG_CRC_ACTIVE : constant := 1;  --  messages.h:771
   --  unsupported macro: SC_MAX_POST_START_WAIT (60 * TX_TIMER_TICKS_PER_SECOND)
   --  unsupported macro: SC_MAX_RUN_START_WAIT (60 * TX_TIMER_TICKS_PER_SECOND)

   SC_POST_RUN_HIGH_PRIORITY : constant := 0;  --  messages.h:776
   SC_POST_RUN_LOW_PRIORITY : constant := 15;  --  messages.h:777

   DATA_LOG_SC_NAME : aliased constant String := "DATA LOG" & ASCII.NUL;  --  messages.h:780
   DATA_LOG_SC_POST_START_PRIORITY : constant := 4;  --  messages.h:781
   DATA_LOG_SC_RUN_START_PRIORITY : constant := 2;  --  messages.h:782
   --  unsupported macro: DL_EVENT_MESSAGE_MAX (sizeof(data_log_msg) - sizeof(mm_xfer_info))
   --  unsupported macro: DL_EVENT_MESSAGE_MIN (DL_EVENT_MESSAGE_MAX - sizeof(uint8_t) + (DL_NUM_32BIT_VAL * sizeof(uint32_t)))

   type sensor_ids is 
     (TH_AIN0_POD_CANOPY_BLOCK_TEMP1,
      TH_AIN1_POD_CANOPY_BLOCK_TEMP2,
      TH_AIN2_POD_CANOPY_OUTLET_TEMP1,
      TH_AIN3_POD_CANOPY_OUTLET_TEMP2,
      TH_AIN4_POD_CANOPY_AIR_TEMP1,
      TH_AIN5_POD_CANOPY_AIR_TEMP2,
      TH_AIN6_BLOOD_CANOPY_BLOCK_TEMP,
      TH_AIN7_BLOOD_CANOPY_OUTLET_TEMP,
      TH_AIN8_BLOOD_CANOPY_AIR_TEMP1,
      TH_AIN9_BLOOD_CANOPY_AIR_TEMP2,
      TH_AIN10_EXTRA1,
      TH_AIN11_EXTRA2,
      TH_AIN12_EXTRA3,
      TH_AIN13_EXTRA4,
      TH_AIN14_EXTRA5,
      TH_AIN15_EXTRA6,
      TH_AIN16_EXTRA7,
      TH_AIN0_PSS_BLOCK_TEMP1,
      TH_AIN1_PSS_BLOCK_TEMP2,
      TH_AIN2_PSS_POST_HEATER_TEMP,
      TH_AIN3_PSS_EOL_TEMP1,
      TH_AIN4_PSS_EOL_TEMP2,
      TH_AIN5_POD_TEMP1,
      TH_AIN6_POD_TEMP2,
      TH_AIN7_POD_TEMP3,
      TH_AIN8_POD_TEMP4,
      TH_AIN9_EXTRA8,
      TH_AIN10_EXTRA9,
      TH_AIN11_UNUSED1,
      TH_AIN12_UNUSED2,
      TH_AIN13_UNUSED3,
      TH_AIN14_UNUSED4,
      TH_AIN15_UNUSED5,
      TH_AIN16_UNUSED6,
      PS_AIN0_WASTE_3,
      PS_AIN1_SOURCE_1,
      PS_AIN2_SOURCE_2,
      PS_AIN3_SOURCE_3,
      PS_AIN4_POD_PRESSURE4,
      PS_AIN5_POD_PRESSURE3,
      PS_AIN6_POD_PRESSURE2,
      PS_AIN7_POD_PRESSURE1,
      PS_AIN8_LINE_PRESSURE1,
      PS_AIN9_LINE_PRESSURE2,
      PS_AIN10_LINE_PRESSURE3,
      PS_AIN11_WASTE_1,
      PS_AIN12_WASTE_2,
      PS_AIN13_THERMISTOR1,
      PS_AIN14_THERMISTOR2,
      PS_AIN15_UNUSED1,
      PS_AIN16_UNUSED2,
      BG_AIN0_PRE_OXY_CO2,
      BG_AIN1_POST_OXY_CO2,
      BG_AIN2_PRE_OXY_O2,
      BG_AIN3_POST_OXY_O2,
      BG_AIN4_PRESSURE_ARTERIAL,
      BG_AIN5_PRESSURE_VENOUS,
      BG_AIN6_PRESSURE_OFFSET,
      BG_AIN7_UNUSED1,
      BG_AIN8_UNUSED2,
      BG_AIN9_UNUSED3,
      BG_AIN10_UNUSED4,
      BG_AIN11_UNUSED5,
      BG_AIN12_UNUSED6,
      BG_AIN13_UNUSED7,
      BG_AIN14_UNUSED8,
      BG_AIN15_UNUSED9,
      BG_AIN16_UNUSED10,
      BG_GAS_MEASURE_ERROR,
      BG_SPECTRUM_WEIGHT,
      BG_SPECTRUM_HB,
      BG_SPECTRUM_PRESAT,
      BG_SPECTRUM_POSTSAT,
      PS_SONOFLOW_FLOW_UL_PER_SEC_S32,
      PS_SONOFLOW_FLOW_ML_PER_MIN_S32,
      PS_SONOFLOW_FLOW_UL_PER_SEC_F32,
      PS_SONOFLOW_FLOW_ML_PER_MIN_F32,
      PS_SONOFLOW_CH1_FLOW_UL_PER_SEC,
      PS_SONOFLOW_CH2_FLOW_UL_PER_SEC,
      PS_SONOFLOW_FLOW_ZERO_ADJ_UL_PER_SEC,
      PS_SONOFLOW_TEMP_IN_CEL,
      PS_SONOFLOW_AVG_TOF_BACKWARDS,
      PS_SONOFLOW_AVG_TOF_FORWARDS,
      PS_SONOFLOW_AVG_TOF,
      PS_SONOFLOW_AVG_TOF_DIF,
      PS_SONOFLOW_TOF_BACKWARDS_1ST_HIT,
      PS_SONOFLOW_TOF_FORWARDS_1ST_HIT,
      PS_SONOFLOW_TOF_BACKWARDS_2ND_HIT,
      PS_SONOFLOW_TOF_FORWARDS_2ND_HIT,
      PS_SONOFLOW_TOF_BACKWARDS_3RD_HIT,
      PS_SONOFLOW_TOF_FORWARDS_3RD_HIT,
      PS_SONOFLOW_TOF_STANDARD_DEVIATION,
      PS_SONOFLOW_ACCUM_VOL_UL_MAX_LIM,
      PS_SONOFLOW_ACCUM_VOL_UL,
      PS_SONOFLOW_VOL_UL_DIF_BEF_VOL_RESET,
      PS_SONOFLOW_VOL_TIME_DIF,
      PS_SONOFLOW_CURRENT_OUTPUT,
      PS_SONOFLOW_FREQUENCY_OUTPUT,
      PS_SONOFLOW_DAC_A_TRIGGER_VAL,
      PS_SONOFLOW_DAC_B_AMP_GAIN,
      PS_SONOFLOW_OUTPUT_FLAGS,
      PS_SONOFLOW_DEVICE_ERROR,
      PS_SONOFLOW_FLOW_MEAS_ERROR,
      PS_SONOFLOW_WARNINGS,
      PS_SONOFLOW_CH1_FLOW_MEAS_ERROR,
      PS_SONOFLOW_CH2_FLOW_MEAS_ERROR,
      PS_SONOFLOW_FLOW_MEAS_MODE,
      PS_SONOFLOW_INT_CLOCK_MAIN_TIME)
   with Convention => C;  -- messages.h:499

   --  skipped anonymous struct anon_anon_2

   type mm_xfer_info is record
      xfer_type : aliased Interfaces.Unsigned_16;  -- messages.h:788
      unused : aliased Interfaces.Unsigned_16;  -- messages.h:789
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:790

   --  skipped anonymous struct anon_anon_3

   type anon_anon_4 is record
      msg_type : Extensions.Unsigned_14;  -- messages.h:799
      reserved : Extensions.Unsigned_1;  -- messages.h:800
      crc_active : Extensions.Unsigned_1;  -- messages.h:801
   end record
     with Convention => C_Pass_By_Copy;
   pragma pack(anon_anon_4) ;
   type mm_hdr is record
      dst : aliased Interfaces.Unsigned_8;  -- messages.h:795
      src : aliased Interfaces.Unsigned_8;  -- messages.h:796
      msg_field : aliased anon_anon_4;  -- messages.h:802
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:803

   --  skipped anonymous struct anon_anon_5

   type mm_word_alignment_array961 is array (0 .. 15) of aliased Interfaces.Unsigned_32;
   type mm_word_alignment is record
      message : aliased mm_word_alignment_array961;  -- messages.h:809
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:810

   --  skipped anonymous struct anon_anon_6

   type mm_int_queue_msg_array966 is array (0 .. 53) of aliased Interfaces.Unsigned_8;
   type mm_int_queue_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:815
      hdr : aliased mm_hdr;  -- messages.h:816
      msg : aliased mm_int_queue_msg_array966;  -- messages.h:817
      crc16 : aliased Interfaces.Unsigned_16;  -- messages.h:818
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:819

   subtype ssmm_overlay_msg is mm_int_queue_msg;  -- messages.h:822

   --  skipped anonymous struct anon_anon_7

   type anon_anon_9 is record
      seq_num : aliased Interfaces.Unsigned_16;  -- messages.h:833
      sys_clock : aliased Interfaces.Unsigned_32;  -- messages.h:834
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_10 is record
      thread_id : aliased Interfaces.Unsigned_16;  -- messages.h:838
      result : aliased Interfaces.Unsigned_16;  -- messages.h:839
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_8 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            echo : aliased anon_anon_9;  -- messages.h:835
         when others =>
            post : aliased anon_anon_10;  -- messages.h:840
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type sys_check_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:827
      hdr : aliased mm_hdr;  -- messages.h:828
      field_3 : aliased anon_anon_8;
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:842

   --  skipped anonymous struct anon_anon_11

   type anon_anon_13 is record
      post_result : aliased Interfaces.Unsigned_16;  -- messages.h:853
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_12 (discr : unsigned := 0) is record
      case discr is
         when others =>
            eresp : aliased anon_anon_13;  -- messages.h:854
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type sys_check_board_hc_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:847
      hdr : aliased mm_hdr;  -- messages.h:848
      field_3 : aliased anon_anon_12;
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:856

   --  skipped anonymous struct anon_anon_14

   type alarm_ver_hdr is record
      alarm_ver_maj : aliased Interfaces.Unsigned_8;  -- messages.h:859
      alarm_ver_min : aliased Interfaces.Unsigned_8;  -- messages.h:860
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:861

   --  skipped anonymous struct anon_anon_15

   type alarm_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:867
      hdr : aliased mm_hdr;  -- messages.h:868
      alarm_id : aliased Interfaces.Unsigned_16;  -- messages.h:869
      action : aliased Interfaces.Unsigned_16;  -- messages.h:870
      board_id : aliased Interfaces.Unsigned_8;  -- messages.h:871
      pri : aliased Interfaces.Unsigned_16;  -- messages.h:872
      aud : aliased Interfaces.Unsigned_16;  -- messages.h:873
      vis : aliased Interfaces.Unsigned_16;  -- messages.h:874
      acked : aliased Extensions.bool;  -- messages.h:875
      quiesce_sec : aliased Interfaces.Unsigned_32;  -- messages.h:876
      alm_hdr : aliased alarm_ver_hdr;  -- messages.h:877
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:878

   type AudioEventEnum is 
     (play_tone,
      stop_tone,
      sample_done,
      alarm_update,
      test_tone,
      volume_set,
      mute,
      unmute,
      automute)
   with Convention => C;  -- messages.h:881

   type AudioEvent is record
      event_enum : aliased AudioEventEnum;  -- messages.h:897
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:895

   --  skipped anonymous struct anon_anon_16

   type audio_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:903
      hdr : aliased mm_hdr;  -- messages.h:904
      aud : aliased Interfaces.Unsigned_16;  -- messages.h:905
      event : aliased AudioEvent;  -- messages.h:906
      duration_ms : aliased Interfaces.Unsigned_32;  -- messages.h:907
      volume : aliased double;  -- messages.h:908
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:909

   --  skipped anonymous struct anon_anon_17

   type visual_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:914
      hdr : aliased mm_hdr;  -- messages.h:915
      vis : aliased Interfaces.Unsigned_16;  -- messages.h:916
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:917

   --  skipped anonymous struct anon_anon_18

   type anon_anon_20 is record
      seq_num : aliased Interfaces.Unsigned_16;  -- messages.h:928
      sys_clock : aliased Interfaces.Unsigned_32;  -- messages.h:929
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_21 is record
      thread_id : aliased Interfaces.Unsigned_16;  -- messages.h:933
      result : aliased Interfaces.Unsigned_16;  -- messages.h:934
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_19 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            echo : aliased anon_anon_20;  -- messages.h:930
         when others =>
            post : aliased anon_anon_21;  -- messages.h:935
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type mm_out_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:922
      hdr : aliased mm_hdr;  -- messages.h:923
      field_3 : aliased anon_anon_19;
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:937

   --  skipped anonymous struct anon_anon_22

   type manager_state_change_info is record
      current_state : aliased Interfaces.Unsigned_8;  -- messages.h:942
      last_state : aliased Interfaces.Unsigned_8;  -- messages.h:943
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:944

   --  skipped anonymous struct anon_anon_23

   type alicat_meter_info_t is record
      id : aliased char;  -- messages.h:949
      flow : aliased float;  -- messages.h:950
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:951
   pragma pack(alicat_meter_info_t);
   --  skipped anonymous struct anon_anon_24

   type alicat_controller_info_t is record
      id : aliased char;  -- messages.h:956
      flow : aliased float;  -- messages.h:957
      setpoint : aliased float;  -- messages.h:958
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:959
   pragma pack(alicat_controller_info_t);
   
   --  skipped anonymous struct anon_anon_25

   type data_log_msg_array1013 is array (0 .. 9) of aliased Interfaces.Unsigned_32;

   
   type data_log_msg_array1015 is array (0 .. 4) of aliased double;

   
   subtype data_log_msg_array1017 is Interfaces.C.char_array (0 .. 39);
   subtype data_log_msg_array1019 is Interfaces.C.char_array (0 .. 33);
   type anon_anon_26 (discr : unsigned := 0) is record
      case discr is
         --  when 0 =>
         --     values_32bit : aliased data_log_msg_array1013;  -- messages.h:973
         --  when 1 =>
         --     values_64bit : aliased data_log_msg_array1015;  -- messages.h:974
         when 2 =>
            text : aliased data_log_msg_array1017;  -- messages.h:975
         when 3 =>
            state_change_info : aliased manager_state_change_info;  -- messages.h:976
         when 4 =>
            alicat_meter_info : aliased alicat_meter_info_t;  -- messages.h:977
         when 5 =>
            alicat_controller_info : aliased alicat_controller_info_t;  -- messages.h:978
         when others =>
            dl_file_name : aliased data_log_msg_array1019;  -- messages.h:979
      end case;
   end record
   with Convention => C_Pass_By_Copy,
     Unchecked_Union => True;
   pragma pack( anon_anon_26 );
   --for anon_anon_26'Alignment use 1 ;
   
   type anon_anon_27 is record
      month : Extensions.Unsigned_4;  -- messages.h:983
      day : Extensions.Unsigned_5;  -- messages.h:984
      year : Extensions.Unsigned_6;  -- messages.h:985
      hour : Extensions.Unsigned_5;  -- messages.h:986
      minute : Extensions.Unsigned_6;  -- messages.h:987
      second : Extensions.Unsigned_6;  -- messages.h:988
   end record
     with Convention => C_Pass_By_Copy;
   pragma pack(anon_anon_27);
   for anon_anon_27'Size use 32 ;
   
   type data_log_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:964
      hdr : aliased mm_hdr;  -- messages.h:965
      subsystem_id : aliased Interfaces.Unsigned_16;  -- messages.h:966
      event_major : aliased Interfaces.Unsigned_16;  -- messages.h:967
      event_minor : aliased Interfaces.Unsigned_16;  -- messages.h:968
      num_values : aliased Interfaces.Unsigned_8;  -- messages.h:969
      value_format : aliased Interfaces.Unsigned_8;  -- messages.h:970
      field_8 : aliased anon_anon_26;
      event_date_time : aliased anon_anon_27;  -- messages.h:989
      millisecond : aliased Interfaces.Unsigned_16;  -- messages.h:990
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:991
   pragma pack(data_log_msg);
   --  skipped anonymous struct anon_anon_28

   type data_log_ss_specific_msg_array1028 is array (0 .. 3) of aliased Interfaces.Unsigned_16;
   type data_log_ss_specific_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:996
      hdr : aliased mm_hdr;  -- messages.h:997
      num_subsystems : aliased Interfaces.Unsigned_16;  -- messages.h:998
      subsystem : aliased data_log_ss_specific_msg_array1028;  -- messages.h:999
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1000

   --  skipped anonymous struct anon_anon_29

   type ain_data_msg is record
      conversion_data : aliased Interfaces.Unsigned_32;  -- messages.h:1005
      voltage : aliased double;  -- messages.h:1006
      resistance : aliased double;  -- messages.h:1007
      refined_data : aliased double;  -- messages.h:1008
      num_data_samples : aliased Interfaces.Unsigned_64;  -- messages.h:1009
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1010

   --  skipped anonymous struct anon_anon_30

   type sonoflow_data_msg is record
      f32 : aliased float;  -- messages.h:1015
      s32 : aliased integer;  -- messages.h:1016
      u32 : aliased Interfaces.Unsigned_32;  -- messages.h:1017
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1018

   --  skipped anonymous struct anon_anon_31

   type anon_anon_32 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            ain_data : aliased ain_data_msg;  -- messages.h:1028
         when others =>
            sonoflow_data : aliased sonoflow_data_msg;  -- messages.h:1029
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type sensor_data_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1023
      hdr : aliased mm_hdr;  -- messages.h:1024
      sensor_id : aliased Interfaces.Unsigned_16;  -- messages.h:1025
      field_4 : aliased anon_anon_32;
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1031

   type e_sweep_flow_status is 
     (success,
      o2_tooLow,
      o2_tooHigh,
      o2_percent_tooHigh,
      co2_tooLow,
      co2_tooHigh,
      n2_tooLow,
      n2_tooHigh,
      sweep_flow_tooHigh,
      set_failure,
      solenoid_failure,
      alicat_read_failure,
      circuit_break_detected)
   with Convention => C;  -- messages.h:1034

   type e_sweep_flow_state is 
     (idle,
      normal,
      sigh,
      calibrate,
      error)
   with Convention => C;  -- messages.h:1052

   --  skipped anonymous struct anon_anon_33

   type bg_sweep_flow_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1063
      hdr : aliased mm_hdr;  -- messages.h:1064
      O2_setpoint_x10 : aliased Short_integer;  -- messages.h:1065
      CO2_setpoint_x10 : aliased Short_integer;  -- messages.h:1066
      N2_setpoint_x10 : aliased Short_integer;  -- messages.h:1067
      O2_current_x10 : aliased Short_integer;  -- messages.h:1068
      CO2_current_x10 : aliased Short_integer;  -- messages.h:1069
      N2_current_x10 : aliased Short_integer;  -- messages.h:1070
      state : aliased e_sweep_flow_state;  -- messages.h:1071
      status : aliased e_sweep_flow_status;  -- messages.h:1072
      cal_mixO2_percent : aliased float;  -- messages.h:1073
      cal_mixCO2_percent : aliased float;  -- messages.h:1074
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1075

   type calibrate_id is 
     (zero_pressure,
      O2_cal,
      CO2_cal)
   with Convention => C;  -- messages.h:1077

   type calibrate_status is 
     (start,
      calibrating,
      done,
      error)
   with Convention => C;  -- messages.h:1084

   --  skipped anonymous struct anon_anon_34

   type bg_calibrate_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1095
      hdr : aliased mm_hdr;  -- messages.h:1096
      id : aliased calibrate_id;  -- messages.h:1097
      status : aliased calibrate_status;  -- messages.h:1098
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1099

   subtype quantum_measurement_type is unsigned;
   SO2 : constant unsigned := 101;
   Hb : constant unsigned := 102;
   Hct : constant unsigned := 103;
   Cardiac_Index_Weight : constant unsigned := 125;  -- messages.h:1102

   type quantum_measurement_msg_f is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1112
      hdr : aliased mm_hdr;  -- messages.h:1113
      measurement_type : aliased quantum_measurement_type;  -- messages.h:1114
      measurement_value : aliased float;  -- messages.h:1115
      socket : aliased Interfaces.Unsigned_8;  -- messages.h:1116
      sensor_part : aliased Interfaces.Unsigned_8;  -- messages.h:1117
      precision : aliased Interfaces.Unsigned_8;  -- messages.h:1118
      units : aliased Interfaces.Unsigned_8;  -- messages.h:1119
      alarm_code : aliased Interfaces.Unsigned_32;  -- messages.h:1120
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1110

   subtype quantum_measurement_msg is quantum_measurement_msg_f;  -- messages.h:1121

   --  skipped anonymous struct anon_anon_35

   type pss_pump_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1127
      hdr : aliased mm_hdr;  -- messages.h:1128
      pump_id : aliased Interfaces.Unsigned_16;  -- messages.h:1129
      pump_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1130
      pump_operation_value : aliased Interfaces.Unsigned_32;  -- messages.h:1131
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1132

   --  skipped anonymous struct anon_anon_36

   type th_heater_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1137
      hdr : aliased mm_hdr;  -- messages.h:1138
      heater_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1139
      heater_operation_value : aliased double;  -- messages.h:1140
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1141

   --  skipped anonymous struct anon_anon_37

   type anon_anon_38 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            s32 : aliased integer;  -- messages.h:1148
         when 1 =>
            u32 : aliased Interfaces.Unsigned_32;  -- messages.h:1149
         when others =>
            f32 : aliased float;  -- messages.h:1150
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type pss_sonoflow_sensor_operation_value is record
      parent : aliased anon_anon_38;
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1152

   --  skipped anonymous struct anon_anon_39

   type pss_sonoflow_sensor_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1156
      hdr : aliased mm_hdr;  -- messages.h:1157
      sensor_id : aliased Interfaces.Unsigned_16;  -- messages.h:1158
      sensor_operation_value : aliased pss_sonoflow_sensor_operation_value;  -- messages.h:1159
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1160

   --  skipped anonymous struct anon_anon_40

   type pss_pod_cannula_bms_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1165
      hdr : aliased mm_hdr;  -- messages.h:1166
      sensor_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1167
      num_data_samples : aliased Interfaces.Unsigned_32;  -- messages.h:1168
      v_value : aliased Interfaces.Unsigned_16;  -- messages.h:1169
      b_value : aliased Interfaces.Unsigned_16;  -- messages.h:1170
      g_value : aliased Interfaces.Unsigned_16;  -- messages.h:1171
      y_value : aliased Interfaces.Unsigned_16;  -- messages.h:1172
      o_value : aliased Interfaces.Unsigned_16;  -- messages.h:1173
      r_value : aliased Interfaces.Unsigned_16;  -- messages.h:1174
      refined_data : aliased Interfaces.Unsigned_32;  -- messages.h:1175
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1176

   --  skipped anonymous struct anon_anon_41

   type pss_pod_waste_bms_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1181
      hdr : aliased mm_hdr;  -- messages.h:1182
      sensor_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1183
      num_data_samples : aliased Interfaces.Unsigned_32;  -- messages.h:1184
      v_value : aliased Interfaces.Unsigned_16;  -- messages.h:1185
      b_value : aliased Interfaces.Unsigned_16;  -- messages.h:1186
      g_value : aliased Interfaces.Unsigned_16;  -- messages.h:1187
      y_value : aliased Interfaces.Unsigned_16;  -- messages.h:1188
      o_value : aliased Interfaces.Unsigned_16;  -- messages.h:1189
      r_value : aliased Interfaces.Unsigned_16;  -- messages.h:1190
      refined_data : aliased Interfaces.Unsigned_32;  -- messages.h:1191
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1192

   --  skipped anonymous struct anon_anon_42

   type pss_pod_pressure_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1197
      hdr : aliased mm_hdr;  -- messages.h:1198
      pressure_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1199
      state : aliased Interfaces.Unsigned_16;  -- messages.h:1200
      pod_setpoint_pressure : aliased double;  -- messages.h:1201
      pod_measured_pressure : aliased double;  -- messages.h:1202
      eol_temperature : aliased double;  -- messages.h:1203
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1204

   --  skipped anonymous struct anon_anon_43

   type pss_pod_pressure_pump_info_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1209
      hdr : aliased mm_hdr;  -- messages.h:1210
      pressure_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1211
      pss_pumpm_tmc5161_inlet_pump_vmax : aliased Interfaces.Unsigned_32;  -- messages.h:1212
      pss_pumpm_tmc5161_flow : aliased Interfaces.Unsigned_32;  -- messages.h:1213
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1214

   --  skipped anonymous struct anon_anon_44

   type pss_pinch_valve_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1220
      hdr : aliased mm_hdr;  -- messages.h:1221
      pinch_valve_id : aliased Interfaces.Unsigned_16;  -- messages.h:1222
      pinch_valve_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1223
      pinch_valve_on_value : aliased Interfaces.Unsigned_32;  -- messages.h:1224
      pinch_valve_stat_value : aliased Interfaces.Unsigned_32;  -- messages.h:1225
      pinch_valve_sens_value : aliased Interfaces.Unsigned_32;  -- messages.h:1226
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1227

   --  skipped anonymous struct anon_anon_45

   type pss_door_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1232
      hdr : aliased mm_hdr;  -- messages.h:1233
      door_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1234
      door1_value : aliased Interfaces.Unsigned_32;  -- messages.h:1235
      door2_value : aliased Interfaces.Unsigned_32;  -- messages.h:1236
      door3_value : aliased Interfaces.Unsigned_32;  -- messages.h:1237
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1238

   --  skipped anonymous struct anon_anon_46

   type pss_bag_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1243
      hdr : aliased mm_hdr;  -- messages.h:1244
      bag_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1245
      bag_set1_state : aliased Interfaces.Unsigned_32;  -- messages.h:1246
      bag_set2_state : aliased Interfaces.Unsigned_32;  -- messages.h:1247
      bag_set3_state : aliased Interfaces.Unsigned_32;  -- messages.h:1248
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1249

   --  skipped anonymous struct anon_anon_47

   type pss_bag_weight_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1254
      hdr : aliased mm_hdr;  -- messages.h:1255
      bag_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1256
      supply_bag_weight : aliased double;  -- messages.h:1257
      waste_bag_weight : aliased double;  -- messages.h:1258
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1259

   --  skipped anonymous struct anon_anon_48

   type pss_uv_disinfector_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1264
      hdr : aliased mm_hdr;  -- messages.h:1265
      uv_disinfector_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1266
      uv_disinfector_enable : aliased Interfaces.Unsigned_32;  -- messages.h:1267
      uv_disinfector_temp_ok : aliased Interfaces.Unsigned_32;  -- messages.h:1268
      uv_disinfector_leds_ok : aliased Interfaces.Unsigned_32;  -- messages.h:1269
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1270

   --  skipped anonymous struct anon_anon_49

   type pss_drawer_led_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1275
      hdr : aliased mm_hdr;  -- messages.h:1276
      operation : aliased Interfaces.Unsigned_16;  -- messages.h:1277
      led_state : aliased Interfaces.Unsigned_16;  -- messages.h:1278
      reg_addr : aliased Interfaces.Unsigned_8;  -- messages.h:1279
      reg_val : aliased Interfaces.Unsigned_8;  -- messages.h:1280
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1281

   --  skipped anonymous struct anon_anon_50

   type th_pod_temperature_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1286
      hdr : aliased mm_hdr;  -- messages.h:1287
      temperature_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1288
      state : aliased Interfaces.Unsigned_16;  -- messages.h:1289
      pod_setpoint_temperature : aliased double;  -- messages.h:1290
      eol_setpoint_temperature : aliased double;  -- messages.h:1291
      block_setpoint_temperature : aliased double;  -- messages.h:1292
      eol_measured_temperature : aliased double;  -- messages.h:1293
      block_measured_temperature : aliased double;  -- messages.h:1294
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1295

   --  skipped anonymous struct anon_anon_51

   type th_pod_temperature_cli_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1300
      hdr : aliased mm_hdr;  -- messages.h:1301
      temperature_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1302
      sensor_id : aliased Interfaces.Unsigned_16;  -- messages.h:1303
      temperature : aliased double;  -- messages.h:1304
      temperature2 : aliased double;  -- messages.h:1305
      temperature3 : aliased double;  -- messages.h:1306
      temperature4 : aliased double;  -- messages.h:1307
      temperature5 : aliased double;  -- messages.h:1308
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1309

   --  skipped anonymous struct anon_anon_52

   type th_pod_canopy_temperature_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1314
      hdr : aliased mm_hdr;  -- messages.h:1315
      temperature_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1316
      state : aliased Interfaces.Unsigned_16;  -- messages.h:1317
      state_machine : aliased Interfaces.Unsigned_16;  -- messages.h:1318
      sensor : aliased Interfaces.Unsigned_16;  -- messages.h:1319
      canopy_sp_or_measured_temperature : aliased double;  -- messages.h:1320
      outlet_sp_or_measured_temperature : aliased double;  -- messages.h:1321
      block_sp_or_measured_temperature : aliased double;  -- messages.h:1322
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1323

   --  skipped anonymous struct anon_anon_53

   type th_blood_canopy_temperature_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1328
      hdr : aliased mm_hdr;  -- messages.h:1329
      temperature_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1330
      state : aliased Interfaces.Unsigned_16;  -- messages.h:1331
      sensor : aliased Interfaces.Unsigned_16;  -- messages.h:1332
      canopy_measured_temperature : aliased double;  -- messages.h:1333
      outlet_measured_temperature : aliased double;  -- messages.h:1334
      block_measured_temperature : aliased double;  -- messages.h:1335
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1336

   --  skipped anonymous struct anon_anon_54

   type th_blood_canopy_setpoint_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1341
      hdr : aliased mm_hdr;  -- messages.h:1342
      temperature_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1343
      state : aliased Interfaces.Unsigned_16;  -- messages.h:1344
      canopy_setpoint_temperature : aliased double;  -- messages.h:1345
      outlet_setpoint_temperature : aliased double;  -- messages.h:1346
      block_setpoint_temperature : aliased double;  -- messages.h:1347
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1348

   --  skipped anonymous struct anon_anon_55

   type th_canopy_info_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1353
      hdr : aliased mm_hdr;  -- messages.h:1354
      temperature_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1355
      canopy_setpoint_temperature : aliased double;  -- messages.h:1356
      canopy_measured_temperature : aliased double;  -- messages.h:1357
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1358

   --  skipped anonymous struct anon_anon_56

   type anon_anon_58 is record
      fan_reg_addr : aliased Interfaces.Unsigned_16;  -- messages.h:1373
      fan_reg_value : aliased Interfaces.Unsigned_16;  -- messages.h:1374
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_59 is record
      fan_clock_frequency : aliased Interfaces.Unsigned_16;  -- messages.h:1378
      fan_timer_unit : aliased Interfaces.Unsigned_16;  -- messages.h:1379
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_60 is record
      msg_invalid_count : aliased Interfaces.Unsigned_32;  -- messages.h:1383
      open_error_count : aliased Interfaces.Unsigned_32;  -- messages.h:1384
      write_error_count : aliased Interfaces.Unsigned_32;  -- messages.h:1385
      read_error_count : aliased Interfaces.Unsigned_32;  -- messages.h:1386
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_61 is record
      open_error_count : aliased Interfaces.Unsigned_32;  -- messages.h:1390
      start_error_count : aliased Interfaces.Unsigned_32;  -- messages.h:1391
      period_set_error_count : aliased Interfaces.Unsigned_32;  -- messages.h:1392
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_57 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            device_active_value : aliased Interfaces.Unsigned_16;  -- messages.h:1368
         when 1 =>
            fan_direction_value : aliased Interfaces.Unsigned_16;  -- messages.h:1369
         when 2 =>
            fan_rpm : aliased Interfaces.Unsigned_16;  -- messages.h:1370
         when 3 =>
            reg_op : aliased anon_anon_58;  -- messages.h:1375
         when 4 =>
            clock_op : aliased anon_anon_59;  -- messages.h:1380
         when 5 =>
            operation_ct : aliased anon_anon_60;  -- messages.h:1387
         when others =>
            timer_ct : aliased anon_anon_61;  -- messages.h:1393
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type th_pod_canopy_fan_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1363
      hdr : aliased mm_hdr;  -- messages.h:1364
      fan_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1365
      field_4 : aliased anon_anon_57;
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1395

   --  skipped anonymous struct anon_anon_62

   type anon_anon_64 is record
      fan_reg_addr : aliased Interfaces.Unsigned_16;  -- messages.h:1410
      fan_reg_value : aliased Interfaces.Unsigned_16;  -- messages.h:1411
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_65 is record
      fan_clock_frequency : aliased Interfaces.Unsigned_16;  -- messages.h:1415
      fan_timer_unit : aliased Interfaces.Unsigned_16;  -- messages.h:1416
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_66 is record
      msg_invalid_count : aliased Interfaces.Unsigned_32;  -- messages.h:1420
      open_error_count : aliased Interfaces.Unsigned_32;  -- messages.h:1421
      write_error_count : aliased Interfaces.Unsigned_32;  -- messages.h:1422
      read_error_count : aliased Interfaces.Unsigned_32;  -- messages.h:1423
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_67 is record
      open_error_count : aliased Interfaces.Unsigned_32;  -- messages.h:1427
      start_error_count : aliased Interfaces.Unsigned_32;  -- messages.h:1428
      period_set_error_count : aliased Interfaces.Unsigned_32;  -- messages.h:1429
   end record
   with Convention => C_Pass_By_Copy;
   type anon_anon_63 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            device_active_value : aliased Interfaces.Unsigned_16;  -- messages.h:1405
         when 1 =>
            fan_direction_value : aliased Interfaces.Unsigned_16;  -- messages.h:1406
         when 2 =>
            fan_rpm : aliased Interfaces.Unsigned_16;  -- messages.h:1407
         when 3 =>
            reg_op : aliased anon_anon_64;  -- messages.h:1412
         when 4 =>
            clock_op : aliased anon_anon_65;  -- messages.h:1417
         when 5 =>
            operation_ct : aliased anon_anon_66;  -- messages.h:1424
         when others =>
            timer_ct : aliased anon_anon_67;  -- messages.h:1430
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type th_blood_canopy_fan_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1400
      hdr : aliased mm_hdr;  -- messages.h:1401
      fan_operation : aliased Interfaces.Unsigned_16;  -- messages.h:1402
      field_4 : aliased anon_anon_63;
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1432

   --  skipped anonymous struct anon_anon_68

   type sys_notify_set_date_time_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1436
      hdr : aliased mm_hdr;  -- messages.h:1437
      year : aliased Interfaces.Unsigned_32;  -- messages.h:1438
      month : aliased Interfaces.Unsigned_16;  -- messages.h:1439
      day : aliased Interfaces.Unsigned_16;  -- messages.h:1440
      hour : aliased Interfaces.Unsigned_16;  -- messages.h:1441
      minute : aliased Interfaces.Unsigned_16;  -- messages.h:1442
      second : aliased Interfaces.Unsigned_16;  -- messages.h:1443
      millisecond : aliased Interfaces.Unsigned_32;  -- messages.h:1444
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1445

   --  skipped anonymous struct anon_anon_69

   type bg_gas_values_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1452
      hdr : aliased mm_hdr;  -- messages.h:1453
      FiO2 : aliased Interfaces.Unsigned_16;  -- messages.h:1454
      FiCO2 : aliased Interfaces.Unsigned_16;  -- messages.h:1455
      FeO2 : aliased Interfaces.Unsigned_16;  -- messages.h:1456
      FeCO2 : aliased Interfaces.Unsigned_16;  -- messages.h:1457
      O2_consumption_x10 : aliased Interfaces.Unsigned_32;  -- messages.h:1458
      O2_consumption_per_kg_x10 : aliased Interfaces.Unsigned_32;  -- messages.h:1459
      CO2_production_x10 : aliased Interfaces.Unsigned_32;  -- messages.h:1460
      CO2_production_per_kg_x10 : aliased Interfaces.Unsigned_32;  -- messages.h:1461
      RQ_x100 : aliased Interfaces.Unsigned_16;  -- messages.h:1462
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1463

   --  skipped anonymous struct anon_anon_70

   type bg_pressure_values_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1468
      hdr : aliased mm_hdr;  -- messages.h:1469
      bpm : aliased Interfaces.Unsigned_16;  -- messages.h:1470
      systolic : aliased Interfaces.Unsigned_16;  -- messages.h:1471
      diastolic : aliased Interfaces.Unsigned_16;  -- messages.h:1472
      PreMAP : aliased Interfaces.Unsigned_16;  -- messages.h:1473
      PostMAP : aliased Interfaces.Unsigned_16;  -- messages.h:1474
      PreMinusPost : aliased Interfaces.Unsigned_16;  -- messages.h:1475
      FlowMAPRatio_x10 : aliased Interfaces.Unsigned_16;  -- messages.h:1476
      O2_resistance_x10 : aliased Interfaces.Unsigned_16;  -- messages.h:1477
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1478

   --  skipped anonymous struct anon_anon_71
   type raw_pressure_samples_type is array(1..8) of integer ;
   type bg_pressure_point_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1483
      hdr : aliased mm_hdr;  -- messages.h:1484
      time_ms : aliased Interfaces.Unsigned_32;  -- messages.h:1485
      pressure_mmHg_x10 : aliased raw_pressure_samples_type ;  -- messages.h:1486
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1487

   --  skipped anonymous struct anon_anon_72

   type bg_baby_weight_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1492
      hdr : aliased mm_hdr;  -- messages.h:1493
      baby_weight_g : aliased Interfaces.Unsigned_32;  -- messages.h:1494
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1495

   --  skipped anonymous struct anon_anon_73

   type bg_sonoflow_sensor_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1500
      hdr : aliased mm_hdr;  -- messages.h:1501
      flow_current : aliased integer;  -- messages.h:1502
      flow_mean : aliased integer;  -- messages.h:1503
      flow_per_kg : aliased integer;  -- messages.h:1504
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1505

   --  skipped anonymous struct anon_anon_74

   type bg_spectrum_gas_values_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1510
      hdr : aliased mm_hdr;  -- messages.h:1511
      O2_extraction : aliased Interfaces.Unsigned_16;  -- messages.h:1512
      O2_delivery_x10 : aliased Interfaces.Unsigned_16;  -- messages.h:1513
      O2_delivery_per_kg_x10 : aliased Interfaces.Unsigned_16;  -- messages.h:1514
      PreSAT : aliased Interfaces.Unsigned_16;  -- messages.h:1515
      PostSAT : aliased Interfaces.Unsigned_16;  -- messages.h:1516
      Hb : aliased Interfaces.Unsigned_16;  -- messages.h:1517
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1518

   --  skipped anonymous struct anon_anon_75

   type mc_battery_values_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1523
      hdr : aliased mm_hdr;  -- messages.h:1524
      battery_condition : aliased Interfaces.Unsigned_8;  -- messages.h:1525
      battery_status : aliased Interfaces.Unsigned_8;  -- messages.h:1526
      battery_charge_status : aliased Interfaces.Unsigned_8;  -- messages.h:1527
      battery_Seconds : aliased Interfaces.Unsigned_32;  -- messages.h:1528
      battery_minutes_remaining : aliased Interfaces.Unsigned_16;  -- messages.h:1529
      battery_charge_remaining : aliased Interfaces.Unsigned_16;  -- messages.h:1530
      battery_voltage : aliased Interfaces.Unsigned_16;  -- messages.h:1531
      battery_temperature : aliased Interfaces.Unsigned_16;  -- messages.h:1532
      battery_level_percent : aliased Interfaces.Unsigned_16;  -- messages.h:1533
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1534

   type LightType is 
     (visible,
      infrared)
   with Convention => C;  -- messages.h:1537

   --  skipped anonymous struct anon_anon_76

   type mc_light_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1546
      hdr : aliased mm_hdr;  -- messages.h:1547
      light_type : aliased LightType;  -- messages.h:1548
      duty_cycle : aliased Interfaces.Unsigned_8;  -- messages.h:1549
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1550

   --  skipped anonymous struct anon_anon_77

   type mc_light_rsp_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1555
      hdr : aliased mm_hdr;  -- messages.h:1556
      light_type : aliased LightType;  -- messages.h:1557
      status : aliased Interfaces.Unsigned_16;  -- messages.h:1558
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1559

   --  skipped anonymous struct anon_anon_78

   type power_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:1564
      hdr : aliased mm_hdr;  -- messages.h:1565
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1566

   --  skipped anonymous struct anon_anon_79

   type anon_anon_80 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            word : aliased mm_word_alignment;  -- messages.h:1573
         when 1 =>
            inter : aliased mm_int_queue_msg;  -- messages.h:1574
         when 2 =>
            sys_check : aliased sys_check_msg;  -- messages.h:1575
         when 3 =>
            sys_check_board_hc : aliased sys_check_board_hc_msg;  -- messages.h:1576
         when 4 =>
            alarm : aliased alarm_msg;  -- messages.h:1577
         when 5 =>
            audio : aliased audio_msg;  -- messages.h:1578
         when 6 =>
            visual : aliased visual_msg;  -- messages.h:1579
         when 7 =>
            mm_out : aliased mm_out_msg;  -- messages.h:1580
         when 8 =>
            dlog : aliased data_log_msg;  -- messages.h:1581
         when 9 =>
            dlog_ss_specific : aliased data_log_ss_specific_msg;  -- messages.h:1582
         when 10 =>
            sensor_data : aliased sensor_data_msg;  -- messages.h:1583
         when 11 =>
            th_heater : aliased th_heater_msg;  -- messages.h:1584
         when 12 =>
            th_pod_temperature : aliased th_pod_temperature_msg;  -- messages.h:1585
         when 13 =>
            th_pod_temperature_set : aliased th_pod_temperature_cli_msg;  -- messages.h:1586
         when 14 =>
            th_pod_canopy_temperature : aliased th_pod_canopy_temperature_msg;  -- messages.h:1587
         when 15 =>
            th_blood_canopy_temperature : aliased th_blood_canopy_temperature_msg;  -- messages.h:1588
         when 16 =>
            th_blood_canopy_setpoint : aliased th_blood_canopy_setpoint_msg;  -- messages.h:1589
         when 17 =>
            th_canopy_info : aliased th_canopy_info_msg;  -- messages.h:1590
         when 18 =>
            th_pod_canopy_fan : aliased th_pod_canopy_fan_msg;  -- messages.h:1591
         when 19 =>
            th_blood_canopy_fan : aliased th_blood_canopy_fan_msg;  -- messages.h:1592
         when 20 =>
            pss_pump : aliased pss_pump_msg;  -- messages.h:1593
         when 21 =>
            pss_sonoflow_sensor : aliased pss_sonoflow_sensor_msg;  -- messages.h:1594
         when 22 =>
            pss_pod_cannula_bms : aliased pss_pod_cannula_bms_msg;  -- messages.h:1595
         when 23 =>
            pss_pod_waste_bms : aliased pss_pod_waste_bms_msg;  -- messages.h:1596
         when 24 =>
            pss_pod_pressure : aliased pss_pod_pressure_msg;  -- messages.h:1597
         when 25 =>
            pss_pod_pressure_pump_info : aliased pss_pod_pressure_pump_info_msg;  -- messages.h:1598
         when 26 =>
            pss_pinch_valve : aliased pss_pinch_valve_msg;  -- messages.h:1599
         when 27 =>
            pss_door : aliased pss_door_msg;  -- messages.h:1600
         when 28 =>
            pss_bag : aliased pss_bag_msg;  -- messages.h:1601
         when 29 =>
            pss_bag_weight : aliased pss_bag_weight_msg;  -- messages.h:1602
         when 30 =>
            pss_uv_disinfector : aliased pss_uv_disinfector_msg;  -- messages.h:1603
         when 31 =>
            pss_drawer_led : aliased pss_drawer_led_msg;  -- messages.h:1604
         when 32 =>
            sys_notify_set_date_time : aliased sys_notify_set_date_time_msg;  -- messages.h:1605
         when 33 =>
            quantum_measurement : aliased quantum_measurement_msg;  -- messages.h:1606
         when 34 =>
            bg_sweep_flow : aliased bg_sweep_flow_msg;  -- messages.h:1607
         when 35 =>
            bg_calibrate : aliased bg_calibrate_msg;  -- messages.h:1608
         when 36 =>
            bg_gas_values : aliased bg_gas_values_msg;  -- messages.h:1609
         when 37 =>
            bg_pressure_values : aliased bg_pressure_values_msg;  -- messages.h:1610
         when 38 =>
            bg_sonoflow_sensor : aliased bg_sonoflow_sensor_msg;  -- messages.h:1611
         when 39 =>
            bg_spectrum_gas_values : aliased bg_spectrum_gas_values_msg;  -- messages.h:1612
         when 40 =>
            bg_pressure_point : aliased bg_pressure_point_msg;  -- messages.h:1613
         when 41 =>
            bg_baby_weight : aliased bg_baby_weight_msg;  -- messages.h:1614
         when 42 =>
            ssmm_overlay : aliased ssmm_overlay_msg;  -- messages.h:1615
         when 43 =>
            mc_battery_values : aliased mc_battery_values_msg;  -- messages.h:1616
         when 44 =>
            mc_light : aliased mc_light_msg;  -- messages.h:1617
         when 45 =>
            mc_light_response : aliased mc_light_rsp_msg;  -- messages.h:1618
         when others =>
            power : aliased power_msg;  -- messages.h:1619
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type mm_queue_msg is record
      parent : aliased anon_anon_80;
   end record
   with Convention => C_Pass_By_Copy;  -- messages.h:1621

end messages_h;
