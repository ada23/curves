with Ada.Text_Io; Use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_IO ;

with Interfaces ; use Interfaces ;
with Interfaces.C; use Interfaces.C ;
with Interfaces.C.Extensions ; use Interfaces.C.Extensions ;

procedure dlsize is
   type mm_xfer_info is record
      xfer_type : aliased Extensions.Unsigned_16;  -- messages.h:788
      unused : aliased Extensions.Unsigned_16;  -- messages.h:789
   end record
     with Convention => C_Pass_By_Copy;  -- messages.h:790
   type anon_anon_4 is record
      msg_type : Extensions.Unsigned_14;  -- messages.h:799
      reserved : Extensions.Unsigned_1;  -- messages.h:800
      crc_active : Extensions.Unsigned_1;  -- messages.h:801
   end record
     with Convention => C_Pass_By_Copy;
   pragma pack(anon_anon_4) ;
   for anon_anon_4'Alignment use 1 ;
   type mm_hdr is record
      dst : aliased Interfaces.Unsigned_8;  -- messages.h:795
      src : aliased Interfaces.Unsigned_8;  -- messages.h:796
      msg_field : aliased anon_anon_4;  -- messages.h:802
   end record
     with Convention => C_Pass_By_Copy;  -- messages.h:803
   type data_log_msg_array1013 is array (0 .. 9) of aliased Extensions.Unsigned_32;
   type data_log_msg_array1015 is array (0 .. 4) of aliased double;
   subtype data_log_msg_array1017 is Interfaces.C.char_array (0 .. 39);
   subtype data_log_msg_array1019 is Interfaces.C.char_array (0 .. 33);
   type anon_anon_26 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            values_32bit : aliased data_log_msg_array1017;  -- messages.h:973
         when 1 =>
            values_64bit : aliased data_log_msg_array1017;  -- messages.h:974
         when 2 =>
            text : aliased data_log_msg_array1017;  -- messages.h:975
         when others =>
            dl_file_name : aliased data_log_msg_array1019;  -- messages.h:979
      end case;
   end record
   with Convention => C_Pass_By_Copy,
     Unchecked_Union => True;
   pragma pack( anon_anon_26 );
   for anon_anon_26'Alignment use 1 ;
   type anon_anon_27 is record
      month : Extensions.Unsigned_4;  -- messages.h:983
      day : Extensions.Unsigned_5;  -- messages.h:984
      year : Extensions.Unsigned_6;  -- messages.h:985
      hour : Extensions.Unsigned_5;  -- messages.h:986
      minute : Extensions.Unsigned_6;  -- messages.h:987
      second : Extensions.Unsigned_6;  -- messages.h:988
   end record
     with Convention => C_Pass_By_Copy;
   pragma pack(anon_anon_27);
   type data_log_msg is record
      xfer_info : aliased mm_xfer_info;  -- messages.h:964
      hdr : aliased mm_hdr;  -- messages.h:965
      subsystem_id : aliased Extensions.Unsigned_16;  -- messages.h:966
      event_major : aliased Extensions.Unsigned_16;  -- messages.h:967
      event_minor : aliased Extensions.Unsigned_16;  -- messages.h:968
      num_values : aliased Interfaces.Unsigned_8;  -- messages.h:969
      value_format : aliased Interfaces.Unsigned_8;  -- messages.h:970
      field_8 : aliased anon_anon_26;
      event_date_time : aliased anon_anon_27;  -- messages.h:989
      millisecond : aliased Extensions.Unsigned_16;  -- messages.h:990
   end record
     with Convention => C_Pass_By_Copy;  -- messages.h:991
   --for data_log_msg'Alignment use 1 ;
   pragma pack(data_log_msg);
   procedure Diag(name: string ; value : integer) is
   begin
      Put(name); Set_Col(40); Put(value); New_Line;
   end Diag ;
   
begin
   Diag("mm_xfer_info",mm_xfer_info'Size/8);
   Diag("mm_hdr",mm_hdr'Size/8);
   Diag("anon_anon_26",anon_anon_26'Size/8);
   Diag("anon_anon_27",anon_anon_27'Size/8);
   Diag("data_log_msg",data_log_msg'Size/8);   
end dlsize;
