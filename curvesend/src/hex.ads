with System ;
package hex is

   procedure dump( d : System.Address ; len : integer ) ;
   function Image( d : System.Address ; len : integer ) return String ;
   
end hex;
