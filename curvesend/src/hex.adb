with System.Unsigned_Types ; use System.Unsigned_Types;
with Ada.Text_Io; use Ada.Text_Io ;

package body hex is

   hdigit : String := "0123456789abcdef" ;

   function Image( d : System.Address ; len : integer ) return String is
      block : array(1..len) of System.Unsigned_Types.Short_Short_Unsigned ;
      for block'address use d ;
      left, right : integer ;
      blockhex : String(1..len*2) ;
   begin
      for b in 1..len
      loop
         left := Integer(block(b)) / 16 ;
         right := Integer(block(b)) - left * 16 ;
         blockhex( b * 2 - 1 ) := hdigit(1+integer(left)) ;
         blockhex( b * 2 ) := hdigit(1+integer(right)) ;
      end loop ;
      return blockhex ;
   end Image ;

   procedure dump( d : System.Address ; len : integer ) is
      block : array(1..len) of System.Unsigned_Types.Short_Short_Unsigned ;
      for block'address use d ;
      left, right : integer ;
   begin
      for b in 1..len
      loop
         left := Integer(block(b)) / 16 ;
         right := Integer(block(b)) - left * 16 ;
         Put( hdigit(1+integer(left)));
         Put( hdigit(1+integer(right)));
      end loop ;
      New_Line ;
   end dump ;

end hex;
