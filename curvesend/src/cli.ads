with GNAT.Strings ;

package cli is

   VERSION : string := "V01" ;
   NAME : String := "curvesend" ;
   Verbose : aliased boolean ;

   HelpOption : aliased boolean ;
   DebugOption : aliased boolean := false ;
   
   port : aliased integer ;
   host : aliased GNAT.Strings.String_Access ;
      
   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

   function Get(prompt : string) return String ;

end cli ;
