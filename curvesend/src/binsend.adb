with bincli ;
with fuzzer ; use fuzzer ;

procedure binsend is
begin
   bincli.ProcessCommandLine ;
   if bincli.DebugOption
   then
      fuzzer.Debug ;
      return ;
   end if ;
   fuzzer.Verbose := bincli.Verbose ;
   fuzzer.SendBinary( host => bincli.host.all , port => bincli.port , binaryFile => bincli.GetNextArgument ) ;

end binsend;
