package fuzzer is

   verbose : boolean := false ;
   procedure Load(fn : string) ;
   procedure Start( frequency : integer := 25 ;     -- per second
                    host : string := "localhost" ;
                    port : integer := 1024 ) ;
   procedure SendBinary( host : string := "localhost" ;
                         port : integer := 2003 ;
                         binaryFile : string ) ;
   procedure Debug ;
   
end fuzzer;
