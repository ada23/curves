
with Ada.Real_Time.Timing_Events;
with Ada.Text_IO; use Ada.Text_IO;
procedure timingtest is
   use Ada.Real_Time.Timing_Events;
   protected Handler is
      procedure Start;
   private
      procedure Hdlr
        (The_Event : in out Ada.Real_Time.Timing_Events.Timing_Event);
      Ev   : Ada.Real_Time.Timing_Events.Timing_Event;
      Next : Ada.Real_Time.Time;
   end Handler;
   Count : Natural := 0;
   protected body Handler is
      procedure Start is
      begin
         Next := Ada.Real_Time.Clock;
         Hdlr (Ev);
      end Start;
      procedure Hdlr
        (The_Event : in out Ada.Real_Time.Timing_Events.Timing_Event)
      is
         use type Ada.Real_Time.Time;
      begin
         Count := Count + 1;
         Next := Next + Ada.Real_Time.Milliseconds (1);
         Ev.Set_Handler
           (Handler => Hdlr'Unrestricted_Access,
            At_Time => Next);
      end Hdlr;
   end Handler;
   Start : Ada.Real_Time.Time;
begin
   Start := Ada.Real_Time.Clock;
   Handler.Start;
   delay until Ada.Real_Time."+" (Start, Ada.Real_Time.Seconds (1));
   Put_Line ("events achieved:" & Count'Image);
end timingtest ;
