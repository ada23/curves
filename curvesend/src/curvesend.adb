with cli ; use cli ;
with fuzzer ; use fuzzer ;


procedure Curvesend is

begin
   cli.ProcessCommandLine ;
   if cli.DebugOption
   then
      fuzzer.Debug ;
      return ;
   end if ;
   fuzzer.Verbose := cli.Verbose ;
   fuzzer.Load( cli.GetNextArgument ) ;
   fuzzer.Start ( host => cli.host.all , port => cli.port ) ;
   delay 600.0 ;
end Curvesend;
