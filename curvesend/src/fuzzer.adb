with System.Address_Image ; 
with System.Address_Operations; use System.Address_Operations;
with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_IO;
with Ada.Float_Text_Io; use Ada.Float_Text_Io ;
with Ada.Direct_IO ;

with Interfaces ; use Interfaces ;

with Ada.Streams.Stream_Io ;

with Ada.Containers.Vectors ;

with Ada.Real_Time.Delays ;
with Ada.Real_Time.Timing_Events ;

with GNAT.Sockets ;

with messages_h ;

with hex ;

package body fuzzer is

   type sample_type is
      record
         time : float ;
         value : float ;
      end record ;
   subtype sample_idx_type is integer range 1..1024 ;
   package Samples_Pkg is new Ada.Containers.Vectors(Sample_Idx_Type , Sample_Type) ;
   samples : Samples_Pkg.Vector ;
   procedure Load(fn : string) is
      sfile : file_Type ;
      s : sample_type ;
      c : Character ;
   begin
      Open(sfile,In_File,fn) ;
      while not End_Of_File(sfile)
      loop
         Get( sfile , s.time );
         c := ' ' ;
         while c /= ','
         loop
            Get(sfile,c);
         end loop ;
         Get( sfile , s.value ) ;
         Samples_Pkg.Append(samples,s);
      end loop ;
      Close(sfile);
      Put("Loaded "); Put(integer(Samples_Pkg.Length(samples))); Put(" samples from "); Put_Line(fn);
   end Load ;
   
   packetssent : integer := 0 ;
   sampleidx : integer := 0 ;
   -- samplestosend : array (1..8) of float ;
   nextsample : sample_idx_type := sample_idx_type'first ;
   
   sendtimer : Ada.Real_Time.Timing_Events.Timing_Event ;  
   SendFreq : integer := 50 ;
 
   sock : GNAT.Sockets.Socket_Type ;

   
   protected Events is

   procedure SendSamples(Event : in out Ada.Real_Time.Timing_Events.Timing_Event);
  --   with Convention => Timing_Event_Handler ;
   private
      fullmsg : messages_h.mm_queue_msg ;
   end Events ;
   
   procedure StartTimer is
      cadence : Ada.Real_Time.Time_Span := Ada.Real_Time.Milliseconds( 1000 / SendFreq ) ;
      atTime : Ada.Real_TIme.Time := Ada.Real_Time.Clock ;
      use Ada.Real_TIme;
   begin
      -- Ada.Real_Time.Timing_Events.Set_Handler(sendtimer,cadence,Events.sendsamples'access) ;
      Ada.Real_Time.Timing_Events.Set_Handler(sendtimer,at_time => atTime + cadence ,
                                              handler => Events.sendsamples'unrestricted_access) ;
      --Put_Line("Started Timer");
   end StartTimer ;
   
   protected body Events is 

      procedure SendSamples(Event : in out Ada.Real_Time.Timing_Events.Timing_Event) is
         bgm : messages_h.bg_pressure_point_msg ;
         for bgm'Address use fullmsg.parent.bg_pressure_point'Address ;
         msgxmitbytes : Ada.Streams.Stream_Element_Array(1..60) ;
         for msgxmitbytes'Address use fullmsg.parent.bg_pressure_point.hdr'Address ;
         bytessent : Ada.Streams.Stream_ELement_Count ;
         use Ada.Streams ;
         cadence : Ada.Real_Time.Time_Span := Ada.Real_Time.Milliseconds( 1000 / SendFreq ) ;
         atTime : Ada.Real_TIme.Time := Ada.Real_Time.Clock ;
         use Ada.Real_TIme;         
      begin
         Ada.Real_Time.Timing_Events.Set_Handler(event ,at_time => atTime + cadence ,
                                                 handler => Events.sendsamples'unrestricted_access) ;
         --StartTimer ;
         -- Put_Line("Timer Event");
         if packetssent = 0
         then
            bgm.time_ms := 0 ;
            bgm.hdr.dst := messages_h.MM_CON_BOARD_ID ;
            bgm.hdr.src := messages_h.MM_BG_BOARD_ID ;
            bgm.hdr.msg_field.msg_type := messages_h.BG_BOARD_PRESSURE_POINT ;
            bgm.hdr.msg_field.crc_active := messages_h.MM_MSG_CRC_IDLE ;
         end if ;
         
         sampleidx := sampleidx + 1 ;
         bgm.pressure_mmHg_x10(sampleidx) := integer( 10.0 * Samples_Pkg.Element(samples,nextsample).value ) ;
         -- Put(bgm.pressure_mmHg_x10(sampleidx) );
         
         if sampleidx = 8
         then
            bgm.time_ms := bgm.time_ms + Interfaces.Unsigned_32(1000 / SendFreq) ;
            GNAT.Sockets.Send_Socket( sock , msgxmitbytes , bytessent ) ;
            if bytessent /= msgxmitbytes'Length
            then
               Put_Line("Full message did not go through ");
            else
               packetssent := packetssent + 1 ;

               if Verbose
               then
                  Put("Sent "); Put(integer(bytessent)); Put_Line(" bytes");
                  hex.dump( msgxmitbytes'Address , integer(bytessent) ) ;
               end if ;
            end if ;
            if Verbose
            then   
               Put(packetssent); Put("packets sent."); New_Line ;
            end if ;
            
            sampleidx := 1 ;
         end if ;
          
         --samplestosend(sampleidx) := Samples_Pkg.Element(samples,nextsample).value ;
         bgm.pressure_mmHg_x10(sampleidx) := integer( 10.0 * Samples_Pkg.Element(samples,nextsample).value ) ;

         if nextsample = integer(Samples_Pkg.Length(samples))
         then
            nextsample := sample_idx_type'first ;
         else
            nextsample := nextsample + 1 ;
         end if ;
         --StartTimer ;
      end SendSamples ;
   end Events ;
   
   fullmsg : messages_h.mm_queue_msg ;
   procedure SendSamples  is
         bgm : messages_h.bg_pressure_point_msg ;
         for bgm'Address use fullmsg.parent.bg_pressure_point'Address ;
         msgxmitbytes : Ada.Streams.Stream_Element_Array(1..60) ;
         for msgxmitbytes'Address use fullmsg.parent.bg_pressure_point.hdr'Address ;
         bytessent : Ada.Streams.Stream_ELement_Count ;
         use Ada.Streams ;
         cadence : Ada.Real_Time.Time_Span := Ada.Real_Time.Milliseconds( 1000 / SendFreq ) ;
         atTime : Ada.Real_TIme.Time := Ada.Real_Time.Clock ;
         use Ada.Real_TIme;         
      begin

         if packetssent = 0
         then
            bgm.time_ms := 0 ;
            bgm.hdr.dst := messages_h.MM_CON_BOARD_ID ;
            bgm.hdr.src := messages_h.MM_BG_BOARD_ID ;
            bgm.hdr.msg_field.msg_type := messages_h.BG_BOARD_PRESSURE_POINT ;
            bgm.hdr.msg_field.crc_active := messages_h.MM_MSG_CRC_IDLE ;
         end if ;
         
         sampleidx := sampleidx + 1 ;
         bgm.pressure_mmHg_x10(sampleidx) := integer( 10.0 * Samples_Pkg.Element(samples,nextsample).value ) ;        
         
         if sampleidx = 8
         then
            bgm.time_ms := bgm.time_ms + Interfaces.Unsigned_32(1000 / SendFreq) ;
            GNAT.Sockets.Send_Socket( sock , msgxmitbytes , bytessent ) ;
            if bytessent /= msgxmitbytes'Length
            then
               Put_Line("Full message did not go through ");
            else
               packetssent := packetssent + 1 ;

               if Verbose
               then
                  Put("Sent "); Put(integer(bytessent)); Put_Line(" bytes");
                  hex.dump( msgxmitbytes'Address , integer(bytessent) ) ;
               end if ;
            end if ;
            if Verbose
            then   
               Put(packetssent); Put("packets sent."); New_Line ;
            end if ;
            
            sampleidx := 1 ;
         end if ;
          
         bgm.pressure_mmHg_x10(sampleidx) := integer( 10.0 * Samples_Pkg.Element(samples,nextsample).value ) ;

         if nextsample = integer(Samples_Pkg.Length(samples))
         then
            nextsample := sample_idx_type'first ;
         else
            nextsample := nextsample + 1 ;
         end if ;
       
   end SendSamples ;
   
   procedure Start( frequency : integer := 25 ;     -- per second
                    host : string := "localhost" ;
                    port : integer := 1024 ) is
      
      server : GNAT.Sockets.Sock_Addr_Type ;
      use Ada.Real_Time ;
   begin
      SendFreq := frequency ;
      Put("Messages Size "); Put(messages_h.bg_pressure_point_msg'Size/8); New_Line ;
      Put("Full Size     "); Put(messages_h.mm_word_alignment'Size/8); New_Line ;
      begin 
         GNAT.Sockets.Create_Socket( sock ) ;
         Put_Line("Socket Created");
      exception
         when others => Put_Line("Failed to create socket");
      end ;
      
      server.Addr := GNAT.sockets.Inet_Addr(host) ;
      server.Port := GNAT.sockets.Port_Type(port) ;
      begin
         GNAT.sockets.Connect_Socket(sock,server);
      exception
         when others =>
            PUt_Line("Exception connecting");
            raise ;
      end ;
      
      loop
         SendSamples ;
         Ada.Real_Time.Delays.Delay_Until( Ada.Real_Time.Clock + Ada.Real_Time.Milliseconds(40));
      end loop ;
      
   end Start ;
   
   package binrec_io is new Ada.Direct_Io ( messages_h.mm_queue_msg );
   procedure SendBinary( host : string := "localhost" ;
                         port : integer := 2003 ;
                         binaryFile : string ) is
            
      server : GNAT.Sockets.Sock_Addr_Type ;
      use Ada.Real_Time ;
      binfile : binrec_io.File_Type ;
      recs_read : Integer := 0 ;
      rec : messages_h.mm_queue_msg ;
      msgxmitbytes : Ada.Streams.Stream_Element_Array(1..60) ;
      for msgxmitbytes'Address use rec'Address ;
      bytessent : Ada.Streams.Stream_ELement_Count ;
      use Ada.Streams ;
      packetssent : integer := 0 ;
   begin
      GNAT.Sockets.Create_Socket( sock ) ;
      Put_Line("Socket Created");
      server.Addr := GNAT.sockets.Inet_Addr(host) ;
      server.Port := GNAT.sockets.Port_Type(port) ;
      begin
         GNAT.sockets.Connect_Socket(sock,server);
      exception
         when others =>
            PUt_Line("Exception connecting");
            raise ;
      end ;
      binrec_io.Open(binfile,binrec_io.In_File,binaryFile);
      Put(binaryFile); Put_Line(" Opened");
      while not binrec_io.End_Of_File(binfile)
      loop
         binrec_io.Read(binfile,rec);
         recs_read := recs_read + 1 ;
         if recs_read mod 10 = 0
         then
            Put(recs_read) ; Put( " records read" ); New_Line ;
         end if ;
         case rec.parent.sys_check.hdr.msg_field.msg_type is
            when messages_h.DATA_LOG_DATE_TIME_REFERENCE => Put_Line("DATA_LOG_DATE_TIME_REFERENCE");
            when others => 
               if verbose
               then
                  Put("MsgType "); Put( integer(rec.parent.sys_check.hdr.msg_field.msg_type) , base => 16 ) ; New_Line ;
               end if ;
               GNAT.Sockets.Send_Socket( sock , msgxmitbytes , bytessent ) ;
               if bytessent /= msgxmitbytes'Length
               then
                  Put_Line("Full message did not go through ");
               else
                  packetssent := packetssent + 1 ;
                  if packetssent mod 100 = 0 and then Verbose 
                  then
                     Put("Sent "); Put(packetssent); Put_Line(" packets");
                  end if ;
               end if ;             
         end case ;
         
      end loop ;
      binrec_io.Close(binfile) ;
   end SendBinary ;
   
   procedure Debug is
      use messages_h ;
      procedure putcol(w : string ) is
      begin
         Put(w); Set_Col(40);
      end PutCol ;
      dl : data_log_msg ;
      pp : bg_pressure_point_msg ;
      
   begin
      PutCol("mm_hdr"); Put(mm_hdr'Size/8); New_Line;
      PutCol("mm_xfer_info"); Put(mm_xfer_info'Size/8); New_Line;
      PutCol("mm_word_alignment "); Put(mm_word_alignment'Size/8); New_Line ;  -- messages.h:1573
      PutCol("mm_int_queue_msg "); Put(mm_int_queue_msg'Size/8); New_Line ; -- messages.h:1574
      PutCol("sys_check_msg "); Put(sys_check_msg'Size/8); New_Line ;  -- messages.h:1575
      PutCol("sys_check_board_hc_msg "); Put(sys_check_board_hc_msg'Size/8); New_Line;  -- messages.h:1576
      PutCol("alarm_msg "); Put(alarm_msg'Size/8);  New_Line ;-- messages.h:1577
      PutCol("audio_msg "); Put(audio_msg'Size/8);  New_Line ;-- messages.h:1578
      PutCol("visual_msg "); Put(visual_msg'Size/8);  New_Line ;-- messages.h:1579
      PutCol("mm_out_msg "); Put(mm_out_msg'Size/8);  New_Line ;-- messages.h:1580
      PutCol("anon_anon_26"); Put(anon_anon_26'Size/8); New_Line ;
      PutCol("anon_anon_27"); Put(anon_anon_27'Size/8); New_Line ;
      PutCol("data_log_msg "); Put(data_log_msg'Size/8);  New_Line ;-- messages.h:1581
      PutCol("data_log_msg "); Put(data_log_msg'Machine_Size/8);  New_Line ;-- messages.h:1581
    
      PutCol("dl"); Put(dl'Size/8); New_Line ;
      PutCol("Address of dl"); Put(System.Address_Image(dl'Address)); New_Line ;
      PutCol("Address of dl.millisecond"); Put(System.Address_Image(dl.millisecond'Address)); New_LIne;
      PutCol("Address Diff "); Put(System.Address_Image(SubA(dl.millisecond'Address,dl'Address))); New_Line;
      PutCol("data_log_ss_specific_msg "); Put(data_log_ss_specific_msg'Size/8); New_Line ; -- messages.h:1582
      PutCol("sensor_data_msg "); Put(sensor_data_msg'Size/8) ; New_Line ;
      PutCol("th_heater_msg "); Put(th_heater_msg'Size/8);  New_Line ;-- messages.h:1584
      PutCol("th_pod_temperature_msg "); Put(th_pod_temperature_msg'Size/8);  New_Line ;-- messages.h:1585
      PutCol("th_pod_temperature_cli_msg "); Put(th_pod_temperature_cli_msg'Size/8); New_Line ;-- messages.h:1586
      PutCol("th_pod_canopy_temperature_msg "); Put( th_pod_canopy_temperature_msg'Size/8) ; New_Line ;-- messages.h:1587
      PutCol("th_blood_canopy_temperature_msg "); Put(th_blood_canopy_temperature_msg'Size/8);  New_Line ; -- messages.h:1588
      PutCol("th_blood_canopy_setpoint_msg "); Put(th_blood_canopy_setpoint_msg'Size/8); New_Line ; -- messages.h:1589
      PutCol("th_canopy_info_msg "); Put(th_canopy_info_msg'Size/8); New_Line ;  -- messages.h:1590
      PutCol("th_pod_canopy_fan_msg "); Put( th_pod_canopy_fan_msg'Size/8); New_Line ;-- messages.h:1591
      PutCol("th_blood_canopy_fan_msg "); Put( th_blood_canopy_fan_msg'Size/8 ) ; New_Line ;-- messages.h:1592
      PutCol("pss_pump_msg "); Put(pss_pump_msg'Size/8);  New_Line ;-- messages.h:1593
      PutCol("pss_sonoflow_sensor_msg "); Put(pss_sonoflow_sensor_msg'Size/8);  New_Line ;-- messages.h:1594
      PutCol( "pss_pod_cannula_bms_msg "); Put(pss_pod_cannula_bms_msg'Size/8);  New_Line ;-- messages.h:1595
      PutCol("pss_pod_waste_bms_msg "); Put(pss_pod_waste_bms_msg'Size/8);  New_Line ;-- messages.h:1596
      PutCol("pss_pod_pressure_msg "); Put( pss_pod_pressure_msg'Size/8) ; New_Line ;-- messages.h:1597
      
      PutCol("pss_pod_pressure_pump_info_msg"); Put(pss_pod_pressure_pump_info_msg'Size/8); New_Line ;--  pss_pod_pressure_pump_info;
      PutCol("pss_pinch_valve_msg "); Put(pss_pinch_valve_msg'Size/8); New_Line ;--            pss_pinch_valve;
      PutCol("pss_door_msg "); Put(pss_door_msg'Size/8); New_Line ;--                   pss_door;
      PutCol("pss_bag_msg "); Put(pss_bag_msg'Size/8); New_Line ;--                    pss_bag;
      PutCol("pss_bag_weight_msg "); Put(pss_bag_weight_msg'Size/8); New_Line ; --             pss_bag_weight;
      PutCol("pss_uv_disinfector_msg "); Put(pss_uv_disinfector_msg'Size/8); New_Line ;--         pss_uv_disinfector;
      PutCol("pss_drawer_led_msg "); Put(pss_drawer_led_msg'Size/8); New_Line ;--             pss_drawer_led;
      PutCol("sys_notify_set_date_time_msg "); Put(sys_notify_set_date_time_msg'Size/8); New_Line ;--   sys_notify_set_date_time;
      PutCol("quantum_measurement_msg "); Put(quantum_measurement_msg'Size/8); New_Line ;--        quantum_measurement;
      PutCol("bg_sweep_flow_msg "); Put(bg_sweep_flow_msg'Size/8); New_Line ;--              bg_sweep_flow;
      PutCol("bg_calibrate_msg "); Put(bg_calibrate_msg'Size/8); New_Line ;--               bg_calibrate;
      PutCol("bg_gas_values_msg "); Put(bg_gas_values_msg'Size/8); New_Line ;--              bg_gas_values;
      PutCol("bg_pressure_values_msg "); Put(bg_pressure_values_msg'Size/8); New_Line ;--         bg_pressure_values;
      PutCol("bg_sonoflow_sensor_msg "); Put(bg_sonoflow_sensor_msg'Size/8); New_Line ;--         bg_sonoflow_sensor;
      PutCol("bg_spectrum_gas_values_msg "); Put(bg_spectrum_gas_values_msg'Size/8); New_Line ;--     bg_spectrum_gas_values;
      PutCol("bg_pressure_point_msg "); Put(bg_pressure_point_msg'Size/8); New_Line ;--          bg_pressure_point;
      
      PutCol("bg_baby_weight_msg "); Put(bg_baby_weight_msg'Size/8); New_Line ;--             bg_baby_weight;
      PutCol("ssmm_overlay_msg "); Put(ssmm_overlay_msg'Size/8); New_Line ;--               ssmm_overlay;
      PutCol("mc_battery_values_msg "); Put(mc_battery_values_msg'Size/8); New_Line ;--          mc_battery_values;
      PutCol("mc_light_msg "); Put(mc_light_msg'Size/8); New_Line ;--                   mc_light;
      PutCol("mc_light_rsp_msg "); Put(mc_light_rsp_msg'Size/8); New_Line ;--                mc_light_response;
      PutCol("power_msg "); Put(power_msg'Size/8); New_Line ; --                      power;
      PutCOl("mm_que_msg"); Put(mm_queue_msg'Size/8); New_Line;
      
      PutCol("pp_msg"); Put(pp'Size/8) ; New_Line ;
      PutCol("pp_msg'Address"); Put(System.Address_Image(pp'Address)) ; New_Line ;     
      
      PutCol("pp_msg.pressure_mmHg_x10"); Put(System.Address_Image(pp.pressure_mmHg_x10(1)'Address)); New_Line ;
      PutCol("pp_msg.pressure_mmHg_x10"); Put(System.Address_Image(pp.pressure_mmHg_x10(2)'Address)); New_Line ;
      
   end Debug ;
   
end fuzzer;
