with Ada.Text_Io; use Ada.Text_Io;
with Ada.Float_Text_IO; use Ada.Float_Text_IO;
with ada.Numerics.Elementary_Functions ; use ada.Numerics.Elementary_Functions ;

with cli ;

procedure Curvegen is
   deltime : float ;
   tpoint : float := 0.0 ;
   val : float ;
   args : array(1..8) of float ;
   argc : integer := 1 ;
begin
   args(1) := 1.0 ;
   cli.ProcessCommandLine ;
   for a in 2..args'Length
   loop
      begin
         args(a) := float'Value( cli.GetNextArgument );
         argc := argc + 1 ;
      exception
         when others => exit ;
      end ;
   end loop ;

   deltime := 1.0 / float(cli.samplingfreq) ;
   while tpoint < float(cli.totaltime)
   loop
      val := 0.0 ;
      for a in 1..argc
      loop
         val := val + float(cli.amplitude) * Sin(2.0*ada.Numerics.pi*float(cli.sigfreq)*tpoint*args(a)) ;
      end loop ;
      Put(tpoint); Put(","); Put(val); New_Line ;
      tpoint := tpoint + deltime ;
   end loop ;
end Curvegen;
