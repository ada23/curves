
package cli is

   VERSION : string := "V01" ;
   NAME : String := "curvegen" ;
   Verbose : aliased boolean ;

   HelpOption : aliased boolean ;
   
   amplitude : aliased integer ;
   sigfreq : aliased integer ;
   
   samplingfreq : aliased integer ;
   totaltime : aliased integer ;
   
   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

   function Get(prompt : string) return String ;

end cli ;
