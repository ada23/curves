#!/bin/bash
set -x
mkdir -p bin
pushd lib
./build.sh
popd
pushd sniff
gprclean -P sniff
gprbuild -p -P sniff
mv obj/sniff ../bin
popd
pushd blog
gprclean -P blog
gprbuild -p -P blog
mv obj/blog ../bin
popd
