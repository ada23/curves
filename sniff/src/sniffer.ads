with System ;
with Interfaces.C ;
with Interfaces.C.Strings ;


with GNAT.Sockets ;
package sniffer is

   verbose : boolean := false ;
   debug : boolean := false ;
   messageTypeFilter : short_integer := 0 ;
   procedure ShowStats ;
   procedure Exec( mc : boolean ;
                   print : boolean ;
                   captureOpt : boolean ;
                   capturefile : string ;
                   messageTypes : short_integer := 0 ); 

   task type ServiceProvider is
      entry Provide( id : integer ; 
                     client : GNAT.Sockets.Socket_Type ) ;
   end ServiceProvider ;
   type ServiceProviderPtr is access ServiceProvider ;

private
   MsgImage : String(1..256);
end sniffer;
