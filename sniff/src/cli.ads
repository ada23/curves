with GNAT.Strings ;

package cli is

   VERSION : string := "V01" ;
   NAME : String := "sniff" ;
   Verbose : aliased boolean ;

   HelpOption : aliased boolean ;
   DebugOption : aliased boolean ;
   printOption : aliased boolean ;
   mcOption : aliased boolean ;
   captureOption : aliased boolean ;
   statsOption : aliased boolean ;
   messageFilter : aliased boolean ;
   
   capture : aliased GNAT.Strings.String_Access ;
   stats : aliased GNAT.Strings.String_Access ;
   messageType : aliased GNAT.Strings.String_Access;
   
   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

   function Get(prompt : string) return String ;

end cli ;
