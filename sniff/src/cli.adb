with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with GNAT.Command_Line;
with GNAT.Source_Info; use GNAT.Source_Info;

package body cli is

   package boolean_text_io is new Enumeration_IO (Boolean);
   use boolean_text_io;

   procedure SwitchHandler
     (Switch : String; Parameter : String; Section : String)
   is
   begin
      Put ("SwitchHandler " & Switch);
      Put (" Parameter " & Parameter);
      Put (" Section " & Section);
      New_Line;
   end SwitchHandler;

   procedure ProcessCommandLine is
      Config : GNAT.Command_Line.Command_Line_Configuration;
   begin

      GNAT.Command_Line.Set_Usage
        (Config,
         Help =>
           NAME & " " & VERSION & " " & Compilation_ISO_Date & " " &
           Compilation_Time,
         Usage => "[options]");

      GNAT.Command_Line.Define_Switch
        (Config, Verbose'access, Switch => "-v", Long_Switch => "--verbose",
         Help                           => "Output extra verbose information");

      GNAT.Command_Line.Define_Switch
        (Config, DebugOption'access, Switch => "-d", Long_Switch => "--debug",
         Help                           => "Debug");

      --  GNAT.Command_Line.Define_Switch
      --    (Config, mcOption'access, Switch => "-m", Long_Switch => "--master-control",
      --     Help                           => "Use the port for MC. (setup IP @ to be MC's). Default is SBC");
  
      GNAT.Command_Line.Define_Switch
        (Config, printOption'access, Switch => "-p", Long_Switch => "--print",
         Help                           => "interpret and print the records");
      
      GNAT.Command_Line.Define_Switch
        (Config, capture'access, Switch => "-c=", Long_Switch => "--capture-file=",
         Help                           => "capture records in binary form in this file");

      GNAT.Command_Line.Define_Switch
        (Config, messageType'access, Switch => "-m=", Long_Switch => "--message-type=",
         Help                           => "message type to filter. Default: all messages");
      
      GNAT.Command_Line.Define_Switch
        (Config, stats'access, Switch => "-stats=", Long_Switch => "--statistics=",
         Help                           => "statistics display. AP for arterial pressure timing");
      
      GNAT.Command_Line.Getopt (Config, SwitchHandler'access);

      if capture.all'Length < 1
      then
         captureOption := false ;
      else
         captureOption := true ;
      end if ;
      
      if messageType.all'Length < 1
      then
         messageFilter := false ;
      else
         messageFilter := true ;
      end if ;
      
      if stats.all'Length < 1
      then
         statsOption := true ;
      else
         statsOption := false ;
      end if ;
      
      if Verbose
      then
         ShowCommandLineArguments ;
      end if ;

   end ProcessCommandLine;

   function GetNextArgument return String is
   begin
      return GNAT.Command_Line.Get_Argument (Do_Expansion => True);
   end GetNextArgument;

   procedure ShowCommandLineArguments is
   begin
      Put ("Verbose ");
      Put (Verbose);
      New_Line ;
      
      Put ("Debug ");
      Put (DebugOption);
      New_Line ;
      
      Put ("MCOption ");
      Put (mcOption);
      New_Line ;
      if captureOption
      then
         Put("Capture File ");
         Put(capture.all) ;
         New_Line ;
      else
         Put_Line("No capture file will be generated");
      end if ;
      
   end ShowCommandLineArguments;

   function Get(Prompt : string) return string is
      result : String(1..80);
      len : natural ;
   begin
      Ada.Text_Io.Put(Prompt) ; Ada.Text_Io.Put(" > "); Ada.Text_Io.Flush ;
      Ada.Text_Io.Get_Line(result,len);
      return result(1..len);
   end Get ;

end cli;
