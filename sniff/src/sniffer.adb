with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_IO ;
with Ada.Float_Text_IO ; use Ada.Float_Text_IO ;
with Ada.Short_Integer_Text_Io; use Ada.Short_Integer_Text_IO ;
with Ada.Streams ;
with GNAT.Strings ;
with Ada.Direct_IO ; 
with ada.Calendar ;
with Ada.Calendar.Arithmetic ;
with INterfaces ; use Interfaces ;

with GNAT.Calendar ;
with GNAT.Sockets;

with messages_h ;

package body sniffer is

   msgsize : constant := 60 ;

   packetsReceived : integer := 0 ;
   packetsIgnored : integer := 0 ;
   connections : integer := 0 ;
   
   prevpacketsReceived : integer := 0 ;
   prevStatsTime : Ada.Calendar.Time := Ada.Calendar.Clock ;
   pktTimeHistogram : array(1..400) of integer := (others => 0) ;
   --task body ServiceProvider is separate ;
   
   procedure ShowStats is
      nowtime : Ada.Calendar.Time := Ada.Calendar.Clock ;
      days : ada.calendar.arithmetic.Day_Count ;
      seconds : duration ;
      leap : ada.calendar.arithmetic.Leap_Seconds_Count ;
      pktarrivalrate : float ;
   begin
      ada.calendar.Arithmetic.Difference(nowtime,prevStatsTime,days,seconds,leap);
     
      pktarrivalrate := float(packetsReceived - prevpacketsReceived) / float(seconds) ;
      prevStatsTime := nowtime ;
      prevpacketsReceived := packetsReceived ;
      Put("Connections "); Put(connections);
      Put(" packets "); Put(packetsReceived, width => 4 ); 
      Put(" ignored "); Put(packetsIgnored , width => 4 ) ;
      Put(" cadence "); Put(pktarrivalrate) ;
      New_Line ;
      Put_Line("Histogram");
      for i in pktTimeHistogram'range
      loop
         if pktTimeHistogram(i) > 0
         then
            Put(i) ;Put(" ");
            Put(pktTimeHistogram(i)) ;
            New_Line ;
         end if ;
      end loop ;
   end ShowStats ;
   
   procedure Exec( mc : boolean ;
                   print : boolean ;
                   captureOpt : boolean ;
                   capturefile : string ;
                   messageTYpes : short_integer := 0 ) is
      Mysocket : GNAT.Sockets.Socket_Type;
      myaddr   : GNAT.Sockets.Sock_Addr_Type;
      mytask   : ServiceProviderPtr;
   begin
      messageTypeFilter := messageTYpes ;
      GNAT.Sockets.Create_Socket (Mysocket);
      myaddr.Addr := GNAT.Sockets.Any_Inet_Addr;
      myaddr.Port := GNAT.Sockets.Port_Type (Messages_h.MM_UI_TCP_PORT);
      GNAT.Sockets.Bind_Socket (Mysocket, myaddr);
      GNAT.Sockets.Listen_Socket (Mysocket);
      loop
         declare
            use GNAT.Sockets;
            clientsocket : GNAT.Sockets.Socket_Type;
            clientaddr   : GNAT.Sockets.Sock_Addr_Type;
            accstatus    : GNAT.Sockets.Selector_Status;
            -- started : GNAT.Calendar.Hour_Number := GNAT.Calendar.Hour(Ada.Calendar.Clock);            
            started : GNAT.Calendar.Minute_Number := GNAT.Calendar.Minute(Ada.Calendar.Clock);
         begin
            Put("Message Types "); Put(messageTypeFilter); Put(messageTypeFilter,base=>16); Put_Line(" will be processed");
            GNAT.Sockets.Accept_Socket
              (Mysocket, clientsocket, clientaddr, 5.0, Status => accstatus);
            if accstatus = GNAT.Sockets.Completed 
            then
               Put ("Client: ");
               Put (GNAT.Sockets.Image (clientaddr));
               Put_Line(" connected");
               connections := connections + 1 ;               
               mytask      := new ServiceProvider;
               mytask.Provide (connections , clientsocket);
            else
               if Verbose
               then
                  Put_Line("Server timer");
               end if ;
               declare
                  -- hnow : GNAT.Calendar.Hour_Number := GNAT.Calendar.Hour( Ada.Calendar.Clock ) ;
                  hnow : GNAT.Calendar.Minute_Number := GNAT.Calendar.Minute( Ada.Calendar.Clock ) ;
               begin
                  if hnow /= started
                  then
                     ShowStats ;
                     started := hnow ;
                  end if ;
               end ;
            end if ;
         end ;
      end loop ;
   end exec ;
   
task body ServiceProvider is
   use Ada.Streams ;

   package Unsigned_8_Text_Io is new Ada.Text_Io.Modular_IO( Interfaces.Unsigned_8 ) ;
   package Unsigned_16_Text_Io is new Ada.Text_Io.Modular_Io( Interfaces.Unsigned_16 ) ;

   myid     : Integer ;
   myidstr : GNAT.Strings.String_Access ;
   myclient : GNAT.Sockets.Socket_Type;
   msgbuffer : messages_h.mm_queue_msg ; -- mm_word_alignment ;
   msglen   : Integer;
   msgbytes : Stream_Element_Array(1 .. Stream_Element_Count(msgsize) ) ;
   for msgbytes'Address use msgbuffer.parent.word.message(1)'Address ;
   stream : GNAT.Sockets.Stream_Access ;
   lastpktTs : Unsigned_32 := 0;
   -- messages : integer := 0 ;
   procedure Process_Message is
      timegap : Unsigned_32 ;
      use Unsigned_8_Text_Io ;
      use Unsigned_16_Text_Io;
      MsgUsedLen : Integer ;
   begin

      if messageTypeFilter > 0 and messageTypeFilter = short_Integer(msgbuffer.parent.power.hdr.msg_field.msg_type)
      then

         if Verbose
         then
            Put(myidstr.all); Put(" > ");
            Put(packetsReceived,width=>5); Put(" > ");
            Put(" Dst : "); Put( Integer(msgbuffer.parent.power.hdr.dst) , width => 3  );
            Put(" Src : "); Put( Integer(msgbuffer.parent.power.hdr.src) , width => 3  );
            Put(" Msg : "); Put( Integer(msgbuffer.parent.power.hdr.msg_field.msg_type) , base => 16 , width => 6) ;
            New_Line ;
         end if ;

         case msgbuffer.parent.power.hdr.msg_field.msg_type is
            when messages_h.BG_BOARD_PRESSURE_POINT =>
               timegap := msgbuffer.parent.bg_pressure_point.time_ms - lastpktTs ;
               lastpktTs := msgbuffer.parent.bg_pressure_point.time_ms ;
               if timegap >= 1 and timegap <= pktTimeHistogram'Length
               then
                  pktTimeHistogram(Integer(timegap)) := pktTimeHistogram(Integer(timegap)) + 1 ;
               end if ;

            when messages_h.PSS_BAG_ALL_BAG_STATUS =>
               --  Put("ALL_BAG_STATUS ");
               --  Put("Active Bagset "); Put( msgbuffer.parent.pss_all_bags_status.last_active_bag_set_id ); Put(" ");
               --  Put("Desired Flowrate "); Put( msgbuffer.parent.pss_all_bags_status.desired_flow_rate ); Put(" ");
               --  Put("Current Flowrte  "); Put( msgbuffer.parent.pss_all_bags_status.current_flow_rate ); Put(" ");
               --  for bag in msgbuffer.parent.pss_all_bags_status.bag_weights'range
               --  loop
               --     Put("Set #"); Put(bag, width => 2); Put(" (") ;
               --     Put(msgbuffer.parent.pss_all_bags_status.bag_weights(bag).supply_bag_weight); Put(",");
               --     Put(msgbuffer.parent.pss_all_bags_status.bag_weights(bag).waste_bag_weight); Put(") ") ;
               --  end loop ;
               MsgUsedLen := Integer(messages_h.mm_queue_msg_printf(MsgImage'Address, MsgImage'Length , msgbuffer.parent.pss_all_bags_status'Address ) ) ;
               Put_Line(MsgImage(1..Integer(MsgUsedLen) ));

             when others => null ;
         end case ;

      else
         packetsIgnored := packetsIgnored + 1 ;
         if Verbose
         then

            Put("Ignoring msgtype :");
            Put( Integer(msgbuffer.parent.power.hdr.msg_field.msg_type) , base => 16 , width => 6) ;
            New_Line ;
         end if ;
      end if ;

   end Process_Message ;

   procedure Receive_Message is
      msgstart : Stream_Element_Offset := 1 ;
      last : Stream_Element_Offset := msgbytes'First - 1;
   begin
      msglen := 0 ;
      while last < msgbytes'Last
      loop
         GNAT.Sockets.Receive_Socket( myclient , msgbytes( msgstart .. msgbytes'Last ) , last ) ;
         if last >= msgbytes'Last
         then
            exit ;
         end if ;
         msgstart := last + 1 ;
      end loop ;
      -- Put("Received "); Put(integer(last)); Put_Line(" bytes");
      packetsReceived := packetsReceived + 1 ;
   end Receive_Message ;

begin
   accept Provide (id : Integer; client : GNAT.Sockets.Socket_Type) do
      begin
         myid     := id;
         myclient := client;
      end;
   end Provide ;
   myidstr := new String'(integer'image(myid)) ;
   stream := GNAT.Sockets.Stream (myclient);
   loop
      msglen := 0 ;
      while msglen < msgsize
      loop
         Receive_Message ;
         Process_Message ;
         if Verbose
         then
            if packetsReceived mod 10 = 0
            then
               Put(packetsReceived) ;
               Put_Line(" packets received");
            end if ;
         end if ;
      end loop ;
   end loop ;

exception
   when others =>
      Put_Line("Shutting down server");
      GNAT.Sockets.Close_Socket(myclient);
end ServiceProvider ;

end sniffer;
