with cli ;
with sniffer ;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Short_Integer_Text_IO ; use Ada.Short_Integer_Text_IO ;
procedure Sniff is
   mtype : short_Integer ;
begin
   --  Insert code here.
   cli.ProcessCommandLine ;
   sniffer.verbose := cli.Verbose ;
   if cli.messageFilter
   then
      mtype := Short_Integer'Value( cli.messageType.all );
   else
      mtype := 0 ;
   end if ;
   Put(mtype) ; New_Line ;
   sniffer.Exec( cli.mcOption , cli.printOption , cli.captureOption , cli.capture.all , mtype ) ;
end Sniff;
