with Interfaces ;
package body messages_h is

   function MessageName( msgid : Interfaces.C.Extensions.Unsigned_16 ) return String is
   begin
      case msgid is
         when ALARM_SET_REQUEST  => return "ALARM_SET_REQUEST" ; -- : constant := 16#0700#;  --  messages.h:69
         when ALARM_SET_SUCCESS  => return "ALARM_SET_SUCCESS" ; -- : constant := 16#0701#;  --  messages.h:70
         when ALARM_SET_FAILURE  => return "ALARM_SET_FAILURE" ; -- : constant := 16#0702#;  --  messages.h:71
         when ALARM_GET_REQUEST  => return "ALARM_GET_REQUEST" ; -- : constant := 16#0703#;  --  messages.h:72
         when ALARM_GET_SUCCESS  => return "ALARM_GET_SUCCESS" ; -- : constant := 16#0704#;  --  messages.h:73
         when ALARM_GET_FAILURE  => return "ALARM_GET_FAILURE" ; -- : constant := 16#0705#;  --  messages.h:74

         when others => return Interfaces.C.Extensions.Unsigned_16'Image( msgid ) ;
      end case ;

   end MessageName ;

   function BoardName( bid : integer ) return String is
   begin
      case bid is
         when MM_MC_BOARD_ID  => return "MC " ; -- : constant := 16#30#;  --  messages.h:641
         when MM_PS_BOARD_ID  => return "PSS" ; -- : constant := 16#31#;  --  messages.h:642
         when MM_TH_BOARD_ID  => return "TH " ; -- : constant := 16#32#;  --  messages.h:643
         when MM_BG_BOARD_ID  => return "BG " ; -- : constant := 16#33#;  --  messages.h:644
         when MM_CON_BOARD_ID => return "SBC" ;-- : constant := 16#34#;  --  messages.h:645
         when others =>          return "???" ;
      end case ;
   end BoardName ;


end messages_h ;
