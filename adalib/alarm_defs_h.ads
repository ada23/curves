pragma Ada_2012;
pragma Style_Checks (Off);

with Interfaces ; use Interfaces ;

package alarm_defs_h is
   subtype unsigned is Interfaces.Unsigned_32 ;
   

   ALARM_ID_FIRST : constant unsigned := 0;
   
   type Alarm_Ids is
   (
   SYSCHECK_MC_POST_FAILURE , --0;
   SYSCHECK_PS_POST_FAILURE , --1;
   SYSCHECK_TH_POST_FAILURE , --2;
   SYSCHECK_BG_POST_FAILURE , --3;
   SYSCHECK_MC_BOARD_TIMEOUT , --4;
   SYSCHECK_PS_BOARD_TIMEOUT , --5;
   SYSCHECK_TH_BOARD_TIMEOUT , --6;
   SYSCHECK_BG_BOARD_TIMEOUT , --7;
   SYSCHECK_SBC_GUI_BOARD_TIMEOUT , --8;
   SYSCHECK_CON_BOARD_TIMEOUT , --9;
   SYSCHECK_MC_THREAD_TIMEOUT , --10;
   SYSCHECK_PS_THREAD_TIMEOUT , --11;
   SYSCHECK_TH_THREAD_TIMEOUT , --12;
   SYSCHECK_BG_THREAD_TIMEOUT , --13;
   SYSCHECK_THREAD_TIMEOUT , --14;
   PSS_WASTE_MECONIUM_DETECTED , --15;
   PSS_WASTE_BLOOD_DETECTED , --16;
   PSS_CANNULA_BMSM_DETECTED , --17;
   PSS_HIGH_EOL_TEMP , --18;
   PSS_OCCLUSION , --19;
   PSS_FILTER1_CLOG , --20;
   PSS_FILTER2_CLOG , --21;
   PSS_SONOFLOW_FAILURE , --22;
   PSS_LINE_PRESSURE1_FAILURE , --23;
   PSS_LINE_PRESSURE2_FAILURE , --24;
   PSS_LINE_PRESSURE3_FAILURE , --25;
   PSS_POD_PRESSURE1_FAILURE , --26;
   PSS_POD_PRESSURE2_FAILURE , --27;
   PSS_POD_PRESSURE_EXCESSIVE , --28;
   PSS_POD_PRESSURE_OUT_OF_RANGE , --29;
   TH_HIGH_EOL_TEMP , --30;
   TH_LOW_EOL_TEMP , --31;
   TH_PSS_BLOCK1_TEMPERATURE_SENSOR_FAILURE , --32;
   TH_PSS_BLOCK2_TEMPERATURE_SENSOR_FAILURE , --33;
   TH_PSS_EOL1_TEMPERATURE_SENSOR_FAILURE , --34;
   TH_PSS_EOL2_TEMPERATURE_SENSOR_FAILURE , --35;
   TH_PSS_POD1_TEMPERATURE_SENSOR_FAILURE , --36;
   TH_PSS_POD2_TEMPERATURE_SENSOR_FAILURE , --37;
   TH_PSS_POD3_TEMPERATURE_SENSOR_FAILURE , --38;
   TH_PSS_TOO_MANY_TEMPERATURE_SENSOR_FAILURES , --39;
   TH_PSS_POD_TEMPERATURE_OUT_OF_RANGE , --40;
   TH_BCANOPY_BLOCK_TEMPERATURE_SENSOR_FAILURE , --41;
   TH_BCANOPY_OUTLET_TEMPERATURE_SENSOR_FAILURE , --42;
   TH_BCANOPY_CANOPY1_TEMPERATURE_SENSOR_FAILURE , --43;
   TH_BCANOPY_CANOPY2_TEMPERATURE_SENSOR_FAILURE , --44;
   TH_BCANOPY_TOO_MANY_TEMPERATURE_SENSOR_FAILURES , --45;
   TH_PCANOPY_BLOCK1_TEMPERATURE_SENSOR_FAILURE , --46;
   TH_PCANOPY_BLOCK2_TEMPERATURE_SENSOR_FAILURE , --47;
   TH_PCANOPY_OUTLET1_TEMPERATURE_SENSOR_FAILURE , --48;
   TH_PCANOPY_OUTLET2_TEMPERATURE_SENSOR_FAILURE , --49;
   TH_PCANOPY_CANOPY1_TEMPERATURE_SENSOR_FAILURE , --50;
   TH_PCANOPY_CANOPY2_TEMPERATURE_SENSOR_FAILURE , --51;
   TH_PCANOPY_TOO_MANY_TEMPERATURE_SENSOR_FAILURES , --52;
   PSS_BAGM_LOW_PSS , --53;
   PSS_BAGM_NO_PSS , --54;
   PSS_BAGM_DEPLETED_PSS , --55;
   PSS_BAGM_PSS_LEAK_DETECTED , --56;
   PSS_BAGM_PINCH_VALVE_SV1_OPEN_FAIL , --57;
   PSS_BAGM_PINCH_VALVE_SV1_CLOSE_FAIL , --58;
   PSS_BAGM_PINCH_VALVE_SV2_OPEN_FAIL , --59;
   PSS_BAGM_PINCH_VALVE_SV2_CLOSE_FAIL , --60;
   PSS_BAGM_PINCH_VALVE_SV3_OPEN_FAIL , --61;
   PSS_BAGM_PINCH_VALVE_SV3_CLOSE_FAIL , --62;
   PSS_BAGM_PINCH_VALVE_WV1_OPEN_FAIL , --63;
   PSS_BAGM_PINCH_VALVE_WV1_CLOSE_FAIL , --64;
   PSS_BAGM_PINCH_VALVE_WV2_OPEN_FAIL , --65;
   PSS_BAGM_PINCH_VALVE_WV2_CLOSE_FAIL , --66;
   PSS_BAGM_PINCH_VALVE_WV3_OPEN_FAIL , --67;
   PSS_BAGM_PINCH_VALVE_WV3_CLOSE_FAIL , --68;
   MC_BLOOD_CANOPY_OPEN , --69;
   MC_POD_CANOPY_OPEN , --70;
   MC_BATTERY_LOW , --71;
   MC_BATTERY_DEPLETED , --72;
   PSS_UV_DISINFECTOR_1_FAILED , --73;
   PSS_UV_DISINFECTOR_2_FAILED , --74;
   PSS_UV_DISINFECTOR_3_FAILED , --75;
   BG_SONOFLOW_DISCRETE_BUBBLE_ERROR , --76;
   BG_SONOFLOW_BUBBLE_ERROR_DETECTED , --77;
   BG_SONOFLOW_CUMULATIVE_BUBBLE_ERROR , --78;
   BG_SONOFLOW_MICROBUBBLE_BUBBLE_ERROR , --79;
   BG_LOW_POST_SATURATION_DETECTED , --80;
   BG_HIGH_TOTAL_SWEEP_FLOW_DETECTED , --81;
   BG_GAS_CALIBRATION_FAILED , --82;
   BG_BLEND_MISMATCH_DETECTED , --83;
   BG_SWEEP_FLOW_MISMATCH_DETECTED , --84;
   BG_SONOFLOW_FLOWVALUE_CHANGE_DETECTED , --85;
   MM_IN_MESSAGE_CORRUPTION , --86;
   POWER_SIG_2V_048A , --87;
   POWER_SIG_2V_048P , --88;
   POWER_SIG_3P3V , --89;
   POWER_SIG_5V , --90;
   POWER_SIG_5V_A , --91;
   POWER_SIG_10V , --92;
   POWER_SIG_24V , --93;
   TH_PCANOPY_HIGH_TEMPERATURE , --94;
   TH_PCANOPY_LOW_TEMPERATURE , --95;
   TH_BCANOPY_HIGH_TEMPERATURE , --96;
   TH_BCANOPY_LOW_TEMPERATURE , --97;
   BG_SONOFLOW_SENSOR_FAILURE , --98;
   BG_PRE_OXY_ARTERIAL_TRANSDUCER_DISCONNECT , --99;
   BG_POST_OXY_VENOUS_TRANSDUCER_DISCONNECT , --100;
   BG_BLEND_SETPOINT_NEVER_REACHED , --101;
   BG_FLOWRATE_SETPOINT_FAILED , --102;
   PSS_EOL_TEMP_LOW , --103;
   PSS_EOL_TEMP_HIGH , --104;
   WATCHDOG_TIMER_IWDT_CAUSED_RESET , --105;
   MC_BATTERY_NOT_RESPONDING , --106;
   BG_PULSE_RATE_LOW_ACUTE_DURATION_DETECTED , --107;
   BG_PULSE_RATE_LOW_CHRONIC_DURATION_DETECTED , --108;
   BG_PULSE_RATE_HIGH_CHRONIC_DURATION_DETECTED , --109;
   BG_PULSE_RATE_HIGH_ACUTE_DURATION_DETECTED --110;

   );
   
   for Alarm_Ids use
   (
   SYSCHECK_MC_POST_FAILURE => 0,
   SYSCHECK_PS_POST_FAILURE => 1,
   SYSCHECK_TH_POST_FAILURE => 2,
   SYSCHECK_BG_POST_FAILURE => 3,
   SYSCHECK_MC_BOARD_TIMEOUT => 4,
   SYSCHECK_PS_BOARD_TIMEOUT => 5,
   SYSCHECK_TH_BOARD_TIMEOUT => 6,
   SYSCHECK_BG_BOARD_TIMEOUT => 7,
   SYSCHECK_SBC_GUI_BOARD_TIMEOUT => 8,
   SYSCHECK_CON_BOARD_TIMEOUT => 9,
   SYSCHECK_MC_THREAD_TIMEOUT => 10,
   SYSCHECK_PS_THREAD_TIMEOUT => 11,
   SYSCHECK_TH_THREAD_TIMEOUT => 12,
   SYSCHECK_BG_THREAD_TIMEOUT => 13,
   SYSCHECK_THREAD_TIMEOUT => 14,
   PSS_WASTE_MECONIUM_DETECTED => 15,
   PSS_WASTE_BLOOD_DETECTED => 16,
   PSS_CANNULA_BMSM_DETECTED => 17,
   PSS_HIGH_EOL_TEMP => 18,
   PSS_OCCLUSION => 19,
   PSS_FILTER1_CLOG => 20,
   PSS_FILTER2_CLOG => 21,
   PSS_SONOFLOW_FAILURE => 22,
   PSS_LINE_PRESSURE1_FAILURE => 23,
   PSS_LINE_PRESSURE2_FAILURE => 24,
   PSS_LINE_PRESSURE3_FAILURE => 25,
   PSS_POD_PRESSURE1_FAILURE => 26,
   PSS_POD_PRESSURE2_FAILURE => 27,
   PSS_POD_PRESSURE_EXCESSIVE => 28,
   PSS_POD_PRESSURE_OUT_OF_RANGE => 29,
   TH_HIGH_EOL_TEMP => 30,
   TH_LOW_EOL_TEMP => 31,
   TH_PSS_BLOCK1_TEMPERATURE_SENSOR_FAILURE => 32,
   TH_PSS_BLOCK2_TEMPERATURE_SENSOR_FAILURE => 33,
   TH_PSS_EOL1_TEMPERATURE_SENSOR_FAILURE => 34,
   TH_PSS_EOL2_TEMPERATURE_SENSOR_FAILURE => 35,
   TH_PSS_POD1_TEMPERATURE_SENSOR_FAILURE => 36,
   TH_PSS_POD2_TEMPERATURE_SENSOR_FAILURE => 37,
   TH_PSS_POD3_TEMPERATURE_SENSOR_FAILURE => 38,
   TH_PSS_TOO_MANY_TEMPERATURE_SENSOR_FAILURES => 39,
   TH_PSS_POD_TEMPERATURE_OUT_OF_RANGE => 40,
   TH_BCANOPY_BLOCK_TEMPERATURE_SENSOR_FAILURE => 41,
   TH_BCANOPY_OUTLET_TEMPERATURE_SENSOR_FAILURE => 42,
   TH_BCANOPY_CANOPY1_TEMPERATURE_SENSOR_FAILURE => 43,
   TH_BCANOPY_CANOPY2_TEMPERATURE_SENSOR_FAILURE => 44,
   TH_BCANOPY_TOO_MANY_TEMPERATURE_SENSOR_FAILURES => 45,
   TH_PCANOPY_BLOCK1_TEMPERATURE_SENSOR_FAILURE => 46,
   TH_PCANOPY_BLOCK2_TEMPERATURE_SENSOR_FAILURE => 47,
   TH_PCANOPY_OUTLET1_TEMPERATURE_SENSOR_FAILURE => 48,
   TH_PCANOPY_OUTLET2_TEMPERATURE_SENSOR_FAILURE => 49,
   TH_PCANOPY_CANOPY1_TEMPERATURE_SENSOR_FAILURE => 50,
   TH_PCANOPY_CANOPY2_TEMPERATURE_SENSOR_FAILURE => 51,
   TH_PCANOPY_TOO_MANY_TEMPERATURE_SENSOR_FAILURES => 52,
   PSS_BAGM_LOW_PSS => 53,
   PSS_BAGM_NO_PSS => 54,
   PSS_BAGM_DEPLETED_PSS => 55,
   PSS_BAGM_PSS_LEAK_DETECTED => 56,
   PSS_BAGM_PINCH_VALVE_SV1_OPEN_FAIL => 57,
   PSS_BAGM_PINCH_VALVE_SV1_CLOSE_FAIL => 58,
   PSS_BAGM_PINCH_VALVE_SV2_OPEN_FAIL => 59,
   PSS_BAGM_PINCH_VALVE_SV2_CLOSE_FAIL => 60,
   PSS_BAGM_PINCH_VALVE_SV3_OPEN_FAIL => 61,
   PSS_BAGM_PINCH_VALVE_SV3_CLOSE_FAIL => 62,
   PSS_BAGM_PINCH_VALVE_WV1_OPEN_FAIL => 63,
   PSS_BAGM_PINCH_VALVE_WV1_CLOSE_FAIL => 64,
   PSS_BAGM_PINCH_VALVE_WV2_OPEN_FAIL => 65,
   PSS_BAGM_PINCH_VALVE_WV2_CLOSE_FAIL => 66,
   PSS_BAGM_PINCH_VALVE_WV3_OPEN_FAIL => 67,
   PSS_BAGM_PINCH_VALVE_WV3_CLOSE_FAIL => 68,
   MC_BLOOD_CANOPY_OPEN => 69,
   MC_POD_CANOPY_OPEN => 70,
   MC_BATTERY_LOW => 71,
   MC_BATTERY_DEPLETED => 72,
   PSS_UV_DISINFECTOR_1_FAILED => 73,
   PSS_UV_DISINFECTOR_2_FAILED => 74,
   PSS_UV_DISINFECTOR_3_FAILED => 75,
   BG_SONOFLOW_DISCRETE_BUBBLE_ERROR => 76,
   BG_SONOFLOW_BUBBLE_ERROR_DETECTED => 77,
   BG_SONOFLOW_CUMULATIVE_BUBBLE_ERROR => 78,
   BG_SONOFLOW_MICROBUBBLE_BUBBLE_ERROR => 79,
   BG_LOW_POST_SATURATION_DETECTED => 80,
   BG_HIGH_TOTAL_SWEEP_FLOW_DETECTED => 81,
   BG_GAS_CALIBRATION_FAILED => 82,
   BG_BLEND_MISMATCH_DETECTED => 83,
   BG_SWEEP_FLOW_MISMATCH_DETECTED => 84,
   BG_SONOFLOW_FLOWVALUE_CHANGE_DETECTED => 85,
   MM_IN_MESSAGE_CORRUPTION => 86,
   POWER_SIG_2V_048A => 87,
   POWER_SIG_2V_048P => 88,
   POWER_SIG_3P3V => 89,
   POWER_SIG_5V => 90,
   POWER_SIG_5V_A => 91,
   POWER_SIG_10V => 92,
   POWER_SIG_24V => 93,
   TH_PCANOPY_HIGH_TEMPERATURE => 94,
   TH_PCANOPY_LOW_TEMPERATURE => 95,
   TH_BCANOPY_HIGH_TEMPERATURE => 96,
   TH_BCANOPY_LOW_TEMPERATURE => 97,
   BG_SONOFLOW_SENSOR_FAILURE => 98,
   BG_PRE_OXY_ARTERIAL_TRANSDUCER_DISCONNECT => 99,
   BG_POST_OXY_VENOUS_TRANSDUCER_DISCONNECT => 100,
   BG_BLEND_SETPOINT_NEVER_REACHED => 101,
   BG_FLOWRATE_SETPOINT_FAILED => 102,
   PSS_EOL_TEMP_LOW => 103,
   PSS_EOL_TEMP_HIGH => 104,
   WATCHDOG_TIMER_IWDT_CAUSED_RESET => 105,
   MC_BATTERY_NOT_RESPONDING => 106,
   BG_PULSE_RATE_LOW_ACUTE_DURATION_DETECTED => 107,
   BG_PULSE_RATE_LOW_CHRONIC_DURATION_DETECTED => 108,
   BG_PULSE_RATE_HIGH_CHRONIC_DURATION_DETECTED => 109,
   BG_PULSE_RATE_HIGH_ACUTE_DURATION_DETECTED => 110

   );
   
   Alarm_Id_Text : array ( Alarm_Ids'Range ) of String (1..37) :=
     (
  --"0123456789012345678901234567890123456"
    "System Check / MC POST Failure       ",
    "System Check / PS POST Failure       ",
    "System Check / TH POST Failure       ",
    "System Check / BG POST Failure       ",
    "System Check / MC Board Timeout      ",
    "System Check / PS Board Timeout      ",
    "System Check / TH Board Timeout      ",
    "System Check / BG Board Timeout      ",
    "System Check / SBC-GUI Board Timeout ",
    "System Check / CON Board Timeout     ",
    "System Check / MC Thread Timeout     ",
    "System Check / PS Thread Timeout     ",
    "System Check / TH Thread Timeout     ",
    "System Check / BG Thread Timeout     ",
    "System Check Thread Timeout          ",    
    "PSS Waste Meconium Detected          ",
    "PSS Waste Blood Detected             ",
    "PSS Cannula BMSM Detected            ",
    "PSS High EOL Temp                    ",
    "PSS Occlusion                        ",
    "PSS Filter 1 Clog                    ",    
    "PSS Filter 2 Clog                    ",
    "PSS Sonoflow Failure                 ",
    "PSS Line Pressure Sensor 1 Failure   ",
    "PSS Line Pressure Sensor 2 Failure   ",
    "PSS Line Pressure Sensor 3 Failure   ",
    "PSS Pod Pressure Sensor 1 Failure    ",
    "PSS Pod Pressure Sensor 2 Failure    ",
    "PSS Excessive Pod Pressure           ",
    "PSS Pod Pressure Out of Range        ",
    "TH High EOL temp                     ",  
    "TH Low EOL temp                      ",
    "PSS Block 1 Temp Sensor Fail         ",
    "PSS Block 2 Temp Sensor Fail         ",
    "PSS EOL 1 Temp Sensor Fail           ",
    "PSS EOL 2 Temp Sensor Fail           ",
    "PSS POD 1 Temp Sensor Fail           ",
    "PSS POD 2 Temp Sensor Fail           ",
    "PSS POD 3 Temp Sensor Fail           ",
    "PSS Too Many Temp Sensor Failures    ",
    "TH Pod Temperature Out of Range      ",    
    "TH BCanopy Block Temp Sensor Fail    ",
    "TH BCanopy Outlet Temp Sensor Fail   ",
    "TH BCanopy Canopy1 Temp Sensor Fail  ",
    "TH BCanopy Canopy2 Temp Sensor Fail  ",
    "TH BCanopy Too Many Temp Sensor Fail ",
    "TH PCanopy Block1 Temp Sensor Fail   ",
    "TH PCanopy Block2 Temp Sensor Fail   ",
    "TH PCanopy Outlet1 Temp Sensor Fail  ",
    "TH PCanopy Outlet2 Temp Sensor Fail  ",
    "TH PCanopy Canopy1 Temp Sensor Fail  ",    
    "TH PCanopy Canopy2 Temp Sensor Fail  ",
    "TH PCanopy Too Many Temp Sensor Fail ",
    "PSS Low PSS                          ",
    "PSS No PSS                           ",
    "PSS Depleted PSS                     ",
    "PSS PSS Leak Detected                ",
    "PSS BAGM Pinch Valve SV1 Open Fail   ",
    "PSS BAGM Pinch Valve SV1 Close Fail  ",
    "PSS BAGM Pinch Valve SV2 Open Fail   ",
    "PSS BAGM Pinch Valve SV2 Close Fail  ",   
    "PSS BAGM Pinch Valve SV3 Open Fail   ",
    "PSS BAGM Pinch Valve SV3 Close Fail  ",
    "PSS BAGM Waste Valve WV1 Open Fail   ",
    "PSS BAGM Waste Valve WV1 Close Fail  ",
    "PSS BAGM Waste Valve WV2 Open Fail   ",
    "PSS BAGM Waste Valve WV2 Close Fail  ",
    "PSS BAGM Waste Valve WV3 Open Fail   ",
    "PSS BAGM Waste Valve WV3 Close Fail  ",
    "MC Blood Canopy Open                 ",
    "MC Pod Canopy Open                   ",    
    "MC Battery low alarm                 ",
    "MC_Battery Depleted alarm            ",
    "PSS UV Disinfector 1 failed          ",
    "PSS UV Disinfector 2 failed          ",
    "PSS UV Disinfector 3 failed          ",
    "BG Discrete Air Bubble Detected      ",
    "BG Bubble Error Detected             ",
    "BG Cumulative Air Bubble Detected    ",
    "BG Micro Air Bubbles Detected        ",
    "BG Low POST Saturation Detected      ",    
    "BG High Total Sweep Flow Detected    ",
    "BG Gas Calibration Failed            ",
    "BG Gas Blend Mismatch Detected       ",
    "BG Gas Flow Mismatch Detected        ",
    "BG Sonoflow Value Change Detected    ",
    "MM / In Message Corruption           ",
    "2 Volt (048A) Power Failure          ",
    "2 Volt (048P) Power Failure          ",
    "3.3 Volt Power Failure               ",
    "5 Volt Power Failure                 ",    
    "5 Volt (A) Power Failure             ",
    "10 Volt Power Failure                ",
    "24 Volt Power Failure                ",
    "TH Pod Canopy Temperature Too High   ",
    "TH Pod Canopy Temperature Too Low    ",
    "TH Blood Canopy Temperature Too High ",
    "TH Blood Canopy Temperature Too Low  ",
    "BG Sonoflow Sensor Failure           ",
    "BG Pre-Oxygen Arterial Trans Disc    ",
    "BG Post-Oxygen Venous Trans Disc     ",    
    "BG Blend Setpoint Never Reached      ",
    "BG Flow Setpoint Never Reached       ",
    "PSS EOL Temp Low                     ",
    "PSS EOL Temp High                    ",
    "Watchdog Timer (IWDT) Caused Reset   ",
    "MC Battery Not Responding            ",
    "BG acute low pulse rate detected     ",
    "BG chronic low pulse rate detected   ",
    "BG_chronic high pulse rate detected  ",
      "BG acute high pulse rate detected    " );
   
   function Name( id : Unsigned_16 ) return String ;
   function Text( id : Unsigned_16 ) return string ;
   
end alarm_defs_h;
