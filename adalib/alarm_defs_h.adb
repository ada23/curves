package body alarm_defs_h is
   function Name( id : Unsigned_16 ) return String is
      AID : Alarm_Ids := Alarm_Ids'Val( Integer(id) ) ;
   begin
      return AID'Image ;
   end Name ;

   function Text( id : Unsigned_16 ) return string is
      AID : Alarm_Ids := Alarm_Ids'Val( Integer(id) ) ;
   begin
      return Alarm_Id_Text(AID) ;
   end Text ;

end alarm_defs_h ;
