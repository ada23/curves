pragma Ada_2012;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;

package datalog_defs_cpp is

   dl_month : array (0 .. 11) of Interfaces.C.Strings.chars_ptr  -- datalog_defs.cpp:6
   with Import => True, 
        Convention => C, 
        External_Name => "dl_month";

   dl_days_in_month : aliased array (0 .. 11) of aliased int  -- datalog_defs.cpp:12
   with Import => True, 
        Convention => C, 
        External_Name => "dl_days_in_month";

   dl_board_id : array (0 .. 4) of Interfaces.C.Strings.chars_ptr  -- datalog_defs.cpp:18
   with Import => True, 
        Convention => C, 
        External_Name => "dl_board_id";

   dl_ss_id : array (0 .. 29) of Interfaces.C.Strings.chars_ptr  -- datalog_defs.cpp:28
   with Import => True, 
        Convention => C, 
        External_Name => "dl_ss_id";

   dl_event_major : array (0 .. 80) of Interfaces.C.Strings.chars_ptr  -- datalog_defs.cpp:63
   with Import => True, 
        Convention => C, 
        External_Name => "dl_event_major";

   dl_event_minor : array (0 .. 112) of Interfaces.C.Strings.chars_ptr  -- datalog_defs.cpp:149
   with Import => True, 
        Convention => C, 
        External_Name => "dl_event_minor";

end datalog_defs_cpp;
