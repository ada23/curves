package body alarm_h is

   function Action_Text( id : Interfaces.Unsigned_16) return String is
      action : alarm_actions := Alarm_Actions'Val(integer(id)) ;
   begin
      return action'Image ;
   end Action_text ;

end alarm_h ;
