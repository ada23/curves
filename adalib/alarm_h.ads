pragma Ada_2012;
pragma Style_Checks (Off);

with Interfaces ;

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;


with System;

package alarm_h is

   ALARM_ACTIVE_MAX : constant := 64;  --  alarm.h:65
   --  unsupported macro: ALARM_ENTRY_NOT_ACTIVE (alarm_active_entry *)0
   --  unsupported macro: ALARM_ACK_VALID_TIME (8 * TX_TIMER_TICKS_PER_SECOND)

   type alarm_thread_states is 
     (ALARM_PRE_POST_INIT,
      ALARM_POST_READY,
      ALARM_POST,
      ALARM_POST_DONE_INIT,
      ALARM_RUN_READY,
      ALARM_RUN,
      ALARM_BOARD_RESTART_WAIT)
   with Convention => C;  -- alarm.h:13

   type alarm_actions is 
     (ALARM_DECLARE,
      ALARM_QUIESCE,
      ALARM_CLEAR,
      ALARM_GET)
   with Convention => C;  -- alarm.h:25
   function Action_Text( id : Interfaces.Unsigned_16) return String ;
   
   type alarm_priorities is 
     (ALARM_LOW_MED_PRIORITY,
      ALARM_HIGH_PRIORITY)
   with Convention => C;  -- alarm.h:34

   type alarm_audibles is 
     (ALARM_AUDIO_NONE,
      ALARM_AUDIO_SLOW,
      ALARM_AUDIO_FAST,
      ALARM_AUDIO_CONSTANT)
   with Convention => C;  -- alarm.h:41

   type alarm_visuals is 
     (ALARM_VISUAL_NONE,
      ALARM_VISUAL_LED_BLINK,
      ALARM_VISUAL_LED_ON)
   with Convention => C;  -- alarm.h:50

   type alarm_ret_codes is 
     (ALARM_SUCCESS,
      ALARM_FAILURE)
   with Convention => C;  -- alarm.h:58

   --  skipped anonymous struct anon_anon_0

   type alarm_active_entry is record
      pri : aliased alarm_priorities;  -- alarm.h:78
      aud : aliased alarm_audibles;  -- alarm.h:79
      vis : aliased alarm_visuals;  -- alarm.h:80
      acked : aliased Extensions.bool;  -- alarm.h:83
   end record
   with Convention => C_Pass_By_Copy;  -- alarm.h:84

   type Alarm_array857 is array (0 .. 63) of aliased alarm_active_entry;
  
   type Alarm_Ids is
     (
      POST_SUCCESS ,
      POST_FAILURE ) ;
   for Alarm_Ids use
     ( POST_SUCCESS => 5 ,
         POST_FAILURE => 10 ) ;
   
end alarm_h;
