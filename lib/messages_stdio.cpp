#define __STDC_FORMAT_MACROS
#include <stdio.h>
#include "messages.h"
extern "C"
{
int mm_queue_msg_printf(char *buffer, int buflen , mm_queue_msg *m)
{
    int chars=0 ;
    int morechars=0 ;
    //chars = snprintf(buffer,buflen,"%02d %02d %04x " , m->sys_check.hdr.src ,  m->sys_check.hdr.dst ,  m->sys_check.hdr.msg_field.msg_type ) ;
    switch (m->sys_check.hdr.msg_field.msg_type)
    {
      case SENSOR_DATA_REQUEST :
      case SENSOR_DATA_RESPONSE :
      case SENSOR_DATA_PUSH :
        morechars = snprintf(&buffer[chars],buflen-chars-1 , "%04d %lu %llu %f %f %f" , m->sensor_data.sensor_id ,
                             (long unsigned int)m->sensor_data.ain_data.conversion_data , 
                             (long long unsigned int) m->sensor_data.ain_data.num_data_samples ,
                             m->sensor_data.ain_data.refined_data , m->sensor_data.ain_data.resistance , m->sensor_data.ain_data.voltage ) ;
        break ;

      case ALARM_SET_REQUEST :
      case ALARM_SET_SUCCESS :
      case ALARM_SET_FAILURE :
      case ALARM_GET_REQUEST :
      case ALARM_GET_SUCCESS :
      case ALARM_GET_FAILURE :
        morechars = snprintf(&buffer[chars],buflen-chars-1," Id: %04d Action: %02d Board: %02x %1d %2d %2d %1d %6lu %1d %1d " ,
                             m->alarm.alarm_id , m->alarm.action , m->alarm.board_id ,
                             m->alarm.pri , m->alarm.aud , m->alarm.vis ,
                             m->alarm.acked , 
                             (long unsigned int)m->alarm.quiesce_sec , m->alarm.alm_hdr.alarm_ver_maj ,
                             m->alarm.alm_hdr.alarm_ver_min ); //,  m->alarm.unique_id );
        break ;

      case TH_POD_TEMPERATURE_SET_REQUEST :
      case TH_POD_TEMPERATURE_SET_SUCCESS :
      case TH_POD_TEMPERATURE_SET_FAILURE :
      case TH_POD_TEMPERATURE_GET_REQUEST :
      case TH_POD_TEMPERATURE_GET_SUCCESS :
      case TH_POD_TEMPERATURE_GET_FAILURE :
        morechars = snprintf(&buffer[chars],buflen-chars-1,
                             "Operation: %2d State: %2d %f %f %f %f %f %f" ,
                             m->th_pod_temperature.temperature_operation ,
                             m->th_pod_temperature.state ,
                             m->th_pod_temperature.pod_setpoint_temperature ,
                             m->th_pod_temperature.eol_setpoint_temperature ,
                             m->th_pod_temperature.block_setpoint_temperature ,
                             m->th_pod_temperature.eol_measured_temperature ,
                             m->th_pod_temperature.block_measured_temperature ,
                             m->th_pod_temperature.pod_avg_temperature ) ;

        break ;
      case PSS_BAG_ALL_BAG_STATUS :
        morechars = snprintf(&buffer[chars] , buflen-chars-1, 
        "Active Bagset %d Flowrate %4d %4d Set 0 : %6f %6f Set 1 : %6f %6f Set 2 : %6f %6f " , 
        m->pss_all_bags_status.last_active_bag_set_id ,
        m->pss_all_bags_status.desired_flow_rate , m->pss_all_bags_status.current_flow_rate ,
        m->pss_all_bags_status.all_bags[0].supply_bag_weight, m->pss_all_bags_status.all_bags[0].supply_bag_weight ,
        m->pss_all_bags_status.all_bags[1].supply_bag_weight, m->pss_all_bags_status.all_bags[1].supply_bag_weight ,
        m->pss_all_bags_status.all_bags[2].supply_bag_weight, m->pss_all_bags_status.all_bags[2].supply_bag_weight ) ;

        default:
            break;
    }
    return chars + morechars + 1;
}
}
